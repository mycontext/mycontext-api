# mycontext-api

## How to Install

Requirements:

- Node.js

Install dependencies:


```
npm install
```

## Available Scripts

In the project directory, you can run:


```
npm start
```

Runs the app in the development mode.
Open [http://localhost:9000](http://localhost:9000) to view it in the browser, if every thing go well you will see 
### 'Bring it on! MyContext is running.'

## Swagger-UI

On development enviroment the api will serve an utility page Swagger-UI with the
swagger documented endpoints and allow you to generate requests. After sucessfully
starting the api you may on your browser open the following links:

 - API: [http://localhost:9000/api-docs](http://localhost:9000/api-docs)