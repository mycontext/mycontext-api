const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let FileSchema = new Schema({
  fileName: {
    type: String,
  },
  mimeType: {
    type: String,
  },
  fileType: {
    type: String,
  },
  grid_id: {
    type: String,
  },
  uploaded_by: {
    type: String,
  },
  uploaded_on: {
    type: Date,
  },
});

let RecordSchema = new Schema(
  {
    patient: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "user",
    },
    doctor: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "user",
    },
    team: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "team",
    },
    cancer_type: {
      type: String,
    },
    age_at_diagnosis: {
      type: Number,
    },
    cs_tumor_size: {
      type: Number,
    },
    gender: {
      type: String,
    },
    year_of_birth: {
      type: Number,
    },
    date_of_diagnosis: {
      type: Date,
    },
    cweight: {
      type: Number,
    },
    bpressure: {
      type: Number,
    },
    blood_group: {
      type: String,
    },
    visit_date: {
      type: Date,
      default: Date.now,
    },
    outcome: {
      type: String,
    },
    outcome_date: {
      type: Date,
    },
    files: {
      type: [FileSchema],
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("record", RecordSchema, "records");
