const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let bidTransactionSchema = new Schema({
  bid: {
    type: mongoose.Types.ObjectId,
    ref: "bid",
    required: true,
  },
  record: {
    type: mongoose.Types.ObjectId,
    ref: "record",
    required: true,
  },
  buyer: {
    type: mongoose.Types.ObjectId,
    ref: "user",
    required: true,
  },
  seller: {
    type: mongoose.Types.ObjectId,
    ref: "user",
    required: true,
  },
  team: {
    type: mongoose.Types.ObjectId,
    ref: "team",
    required: true,
  },
  comment: {
    type: String,
    required: false,
  },
  status: {
    type: String,
    enum: ["A", "R"],
    required: true,
  },
  amountEarned: {
    type: Number,
    required: false,
  },
});

module.exports = mongoose.model(
  "bidTransaction",
  bidTransactionSchema,
  "bidTransactions"
);
