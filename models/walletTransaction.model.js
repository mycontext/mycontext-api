const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let WalletTransactionSchema = new Schema({
  userId: {
    type: mongoose.Types.ObjectId,
    ref: "user",
    required: true,
  },
  accountHolderName: {
    type: String,
    required: true,
  },
  amount: {
    type: Number,
    required: true,
  },
  accountNumber: {
    type: String,
    required: true,
  },
  bsbNumber: {
    type: String,
    required: true,
  },
  confirmationCode: {
    type: String,
    required: true,
  },
  verified: {
    type: Boolean,
    required: true,
    default: false,
  },
  transferred: {
    type: Boolean,
    required: true,
    default: false,
  },
  transferredDate: {
    type: Date,
    required: false,
  },
});

module.exports = mongoose.model(
  "walletTransaction",
  WalletTransactionSchema,
  "walletTransactions"
);
