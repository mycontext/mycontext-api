const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let SubRecordSchema = new Schema(
  {
    patient: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "user",
    },
    team: { type: mongoose.Schema.Types.ObjectId, required: true, ref: "team" },
    doctor: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "user",
    },
    data: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("subrecord", SubRecordSchema, "subrecords");
