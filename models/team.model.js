const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const prefix = "ContributorsList_";

let ProfitSchema = new Schema({
  user: {
    type: mongoose.Types.ObjectId,
    ref: "user",
    required: true,
  },
  share: {
    type: Number,
    required: true,
    default: 0,
  },
});

let TeamSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    admin: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: "user",
    },
    adminShare: {
      type: Number,
      required: true,
      default: 100,
    },
    members: [ProfitSchema],
  },
  {
    timestamps: true,
  }
);

TeamSchema.pre("save", function (next) {
  var team = this;
  if (!team.isModified("name")) return next();
  team.name = prefix.concat(team.name);
  next();
});

TeamSchema.methods.compareName = function (userMail, cb) {
  cb(this.name.split("ContributorsList_")[1] === userMail);
};

module.exports = mongoose.model("team", TeamSchema, "teams");
