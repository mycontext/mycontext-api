const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let buySchema = new Schema({
    cardName: {
        type: String
    },
    cardNumber: {
        type: String
    },
    expiryMonth: {
        type: String
    },
    expiryYear: {
        type: String
    },
    cvv: {
        type: String
    },
    recordId: {
        type: String
    },
    userId: {
        type: String
    },
});


module.exports = mongoose.model("myrecords", buySchema, "myrecords");
