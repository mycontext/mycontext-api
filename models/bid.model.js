const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Counter = require("./counter.model");
const CONSTANTS = require("../utils/constants");

let BidSchema = new Schema(
  {
    bidTitle: {
      type: String,
      required: true,
    },
    cancerType: {
      type: String,
      required: true,
    },
    recordsRequired: {
      type: Number,
      required: true,
    },
    recordsAcquired: {
      type: Number,
      required: true,
      default: 0,
    },
    bidStartDate: {
      type: Date,
      required: true,
    },
    bidExpiryDate: {
      type: Date,
      required: true,
    },
    amountPerRecord: {
      type: Number,
      required: true,
    },
    bidStatus: {
      type: String,
      enum: CONSTANTS.DB_BIDSTATUS,
      required: true,
    },
    recordAccessStartDate: {
      type: Date,
      required: true,
    },
    recordAccessExpiryDate: {
      type: Date,
      required: true,
    },
    buyer: {
      type: mongoose.Types.ObjectId,
      ref: "user",
      required: true,
    },
    teamId: {
      type: mongoose.Types.ObjectId,
      ref: "team",
      required: false,
    },
    recordBought: {
      type: [{ type: mongoose.Types.ObjectId, ref: "record" }],
      required: false,
    },
    recordAccepted: {
      type: [{ type: mongoose.Types.ObjectId, ref: "record" }],
      required: false,
    },
  },
  {
    timestamps: true,
  }
);

BidSchema.pre("save", function (next) {
  var doc = this;
  Counter.findById("bidId").then((result) => {
    if (!result) {
      const counter = new Counter({ _id: "bidId" }, { seq: 1 });
      counter
        .save()
        .then((data) => {
          doc.bidTitle = doc.bidTitle.concat(data.seq);
          next();
        })
        .catch((error) => next(error));
    } else {
      Counter.findByIdAndUpdate(
        { _id: "bidId" },
        { $inc: { seq: 1 } },
        function (error, counter) {
          if (error) return next(error);
          doc.bidTitle = doc.bidTitle.concat(counter.seq);
          next();
        }
      );
    }
  });
});

module.exports = mongoose.model("bid", BidSchema, "bids");
