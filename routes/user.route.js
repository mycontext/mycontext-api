const express = require("express");
const user_controller = require("../controllers/user.controller");
var verify_token = require("./verify.token");

const router = express.Router();

/**
 * @swagger
 * components:
 *  securitySchemes:
 *      AccessToken:
 *          type: apiKey
 *          in: header
 *          name: x-access-token
 *  schemas:
 *    address:
 *      properties:
 *        street_name:
 *          type: string
 *          description: the strret name of the user
 *        post_code:
 *          type: string
 *          description: the post code of the user
 *        city:
 *          type: string
 *          description: the city of the user
 *        state:
 *          type: string
 *          description: the state of the user
 */

/**
 * @swagger
 * /user/list-users:
 *   post:
 *     tags: ['user']
 *     description: Returns list of all users
 *     produces:
 *       - application/json
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: List of all users
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether users fetched successfully
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message with respect fetching of user list.
 *                          required: true
 *                      data:
 *                          type: object
 *                          properties:
 *                              id:
 *                                  type: string
 *                                  description: user id
 *                              name:
 *                                  type: string
 *                                  description: user name
 *                              email:
 *                                  type: string
 *                                  description: user email id
 *                          description: User object
 *                          required: true
 *       500:
 *         description: Gives the error for failing to retrieve all patients
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether users fetched successfully
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error behind failure of fetching the users
 *                          required: true
 */
router.post("/list-users", verify_token, user_controller.listUsers);

/**
 * @swagger
 * /user/buy-record:
 *   post:
 *     tags: ['user']
 *     description: Buy a record
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *      name: Card Details
 *      description: Card Details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      name:
 *                          type: string
 *                          required: true
 *                          description: Name on the card.
 *                      cardNumber:
 *                          type: string
 *                          required: true
 *                          description: Card number.
 *                      month:
 *                          type: string
 *                          required: true
 *                          description: Expiry month as given on card.
 *                      year:
 *                          type: string
 *                          required: true
 *                          description: Expiry year as given on card.
 *                      cvv:
 *                          type: string
 *                          required: true
 *                          description: cvv as given on card.
 *                      recordId:
 *                          type: string
 *                          required: true
 *                          description: Id of the record being purchased.
 *                      userId:
 *                          type: string
 *                          required: true
 *                          description: Id of the user purchasing the record.
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Status of the payment
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether Payment successful or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message with respect the outcome of Payment.
 *                          required: true
 *       500:
 *         description: Failed status of the payment
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether Payment successful or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message with respect the outcome of Payment.
 *                          required: true
 */
router.post("/buy-record", verify_token, user_controller.buyRecord);

/**
 * @swagger
 * /user/register:
 *   post:
 *     tags: ['user']
 *     description: Register as new user
 *     produces:
 *       - application/json
 *     requestBody:
 *      description: User Details
 *      name: User Details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      email:
 *                          type: string
 *                          required: true
 *                          description: Email with which user has registered.
 *                      name:
 *                          type: string
 *                          required: true
 *                          description: User's name.
 *                      password:
 *                          type: string
 *                          required: true
 *                          description: Password.
 *                      user_type:
 *                          type: string
 *                          required: true
 *                          description: Type of the user which may be doctor, patient and likewise.
 *                      address:
 *                          type: object
 *                          $ref: '#/components/schemas/address'
 *                      abn:
 *                          type: number
 *                          description: it is mandatory if registering as organization
 *                      regsitrationNumber:
 *                          type: string
 *                          description: it is mandatory if registering as doctor
 *                      phone:
 *                          type: string
 *                          description: phone number of the user
 *                      specialization:
 *                          type: string
 *                          description: the specialization of the doctor when registering as doctor
 *     responses:
 *       200:
 *         description: Status of Registration
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether Registration successful or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message with respect the outcome of Registration.
 *                          required: true
 *       500:
 *         description: Status of Registration
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether Registration successful or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message with respect the outcome of Registration.
 *                          required: true
 */
router.post("/register", user_controller.register);

/**
 * @swagger
 * /user/login:
 *   post:
 *     tags: ['user']
 *     description: Login to the application
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *         description: Login credentials
 *         required: true
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      email:
 *                          type: string
 *                          description: Email with which user has registered.
 *                          required: true
 *                      password:
 *                          type: string
 *                          description: User's password.
 *                          required: true
 *     responses:
 *       200:
 *         description: Status of login
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                       success:
 *                          type: boolean
 *                          description: Whether login successful or not
 *                          required: true
 *                       message:
 *                          type: string
 *                          description: Message with respect the outcome of authentication. can contain success or respective error message.
 *                          required: true
 *                       token:
 *                          type: string
 *                          description: If login successful the generated access token
 *                       name:
 *                          type: string
 *                          description: The name of the user as given during registration
 *                       email:
 *                          type: string
 *                          description: The email as given during registration
 *                       user_id:
 *                          type: string
 *                          description: The user id generated during registration
 *                       user_type:
 *                          type: string
 *                          description: The type of user like Patient, Doctor and likewise
 *                       teamName:
 *                          type: string
 *                          description: Team name of the patient
 *       500:
 *         description: Status of Login
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether Login successful or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message with respect to the outcome of Login.
 *                          required: true
 */
router.post("/login", user_controller.login);

/**
 * @swagger
 * /user/confirmation/{token}:
 *   get:
 *     tags: ['user']
 *     description: Authentication confirmation based on token
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: Access token.
 *         in: path
 *         required: true
 *         schema:
 *          type: string
 *     responses:
 *       200:
 *         description: Status of the authentication of user
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether access token is valid or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message with respect the outcome of authentication.
 *                          required: true
 *       500:
 *         description: Status of the authentication of user
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether access token is valid or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Erro message with respect the outcome of authentication.
 *                          required: true
 */
router.get("/confirmation/:token", user_controller.confirmation);
/**
 * @swagger
 * /user/forgotPassword:
 *   post:
 *     tags: ['user']
 *     description: Sends a reset link to the user email id
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *         description: User Registered Emiail address
 *         required: true
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      email:
 *                          type: string
 *                          description: Email with which user has registered.
 *                          required: true
 *     responses:
 *       200:
 *         description: Status of the forgot password flow
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether link has been sent or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Success message with email to which link has been sent
 *                          required: true
 *       500:
 *         description: Status of the of forgot password flow
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether link has been sent or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message with respect the outcome of forgot password flow.
 *                          required: true
 */
router.post("/forgotPassword", user_controller.forgotPassword);
/**
 * @swagger
 * /user/changePassword:
 *   post:
 *     tags: ['user']
 *     description: Changes the password of the user
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *         description: User token and new password details
 *         required: true
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      token:
 *                          type: string
 *                          description: Access token of the user
 *                          required: true
 *                      password:
 *                          type: string
 *                          description: The new password that user wants to set
 *                          required: true
 *     responses:
 *       200:
 *         description: Status of the change password flow
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether the password has been changes or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Success message regarding the password change
 *                          required: true
 *       500:
 *         description: Status of the of change password flow
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether the password has been changes or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message regarding the password change
 *                          required: true
 */
router.post("/changePassword", user_controller.changePassword);

/**
 * @swagger
 * /user/addTeamMember:
 *   post:
 *     tags: ['team']
 *     description: Add a new contributor into a patient team
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *         description: Id and profit share of the contributor
 *         required: true
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      membersToBeAdded:
 *                          type: array
 *                          description: List of objects containing ids and percentage share to be added into teams
 *                          required: true
 *                          items:
 *                              type: object
 *                              properties:
 *                                  user:
 *                                      type: string
 *                                      description: user id of the member to be added into the team
 *                                      required: true
 *                                  share:
 *                                      type: number
 *                                      descripton: percentage share to be given to member on selling the record
 *                                      required: false
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Status of the adding the contributor to the team
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether the member has been added or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Success message regarding the adding the new team member
 *                          required: true
 *       500:
 *         description: Status of adding the contributor to the team
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether the member has been added or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message regarding the adding of new team member
 *                          required: true
 */
router.post("/addTeamMember", verify_token, user_controller.addTeamMember);

/**
 * @swagger
 * /user/searchDoctorbyEmailorName:
 *   post:
 *     tags: ['user']
 *     description: search doctors by matching email or name
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *      name: Search doctor by Email address or name
 *      description: Search doctor by Email address or name
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                     mailIdOrName:
 *                          type: string
 *                          required: true
 *                          description: Email address or Name of the doctor whom you want to add.
 *                     from:
 *                          type: integer
 *                          description: From number for pagination
 *                     size:
 *                          type: integer
 *                          description: Size of the return list
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Status of the adding the record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Status of the adding the record
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successful addition of record message
 *                          required: true
 *       500:
 *         description: Status of the adding the record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Status of the adding the record
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error behind addition of record message
 *                          required: true
 */
router.post(
  "/searchDoctorbyEmailorName",
  verify_token,
  user_controller.searchDoctorbyEmailorName
);

/**
 * @swagger
 * /user/addTeamMemberByMailId:
 *   post:
 *     tags: ['team']
 *     description: Add a new contributor into a patient team
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *         description: Id of the contributor
 *         required: true
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                        emailAddress:
 *                          type: string
 *                          description: Email address of the contributor
 *                          required: true
 *                        share:
 *                          type: number
 *                          description: Share the member receives when the record is sold
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Status of the adding the contributor to the team
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether the member has been added or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Success message regarding the adding the new team member
 *                          required: true
 *       500:
 *         description: Status of adding the contributor to the team
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether the member has been added or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message regarding the adding of new team member
 *                          required: true
 */
router.post(
  "/addTeamMemberByMailId",
  verify_token,
  user_controller.addTeamMemberByMailId
);

/**
 * @swagger
 * /user/getCounts:
 *   post:
 *     tags: ['dashboard']
 *     description: get data required for dashboard
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: success
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Data fetched successfully
 *                          required: true
 *                      patientsCount:
 *                          type: integer
 *                          description: Count of patient
 *                          required: true
 *                      doctorCount:
 *                          type: integer
 *                          description: Count of doctor
 *                          required: true
 *                      medicalRecordsCount:
 *                          type: integer
 *                          description: Count of medical records
 *                          required: true
 *                      bidsCount:
 *                          type: integer
 *                          description: Count of bids
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Success message regarding the adding the new team member
 *                          required: true
 *       500:
 *         description: Status of adding the contributor to the team
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether the member has been added or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message regarding the adding of new team member
 *                          required: true
 */
router.post("/getCounts", verify_token, user_controller.getCounts);

/**
 * @swagger
 * /user/getGraphsData:
 *   post:
 *     tags: ['dashboard']
 *     description: get data required for dashboard
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: success
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Data fetched successfully
 *                          required: true
 *                      cancerByType:
 *                          type: array
 *                          description: Contains data of cancer type
 *                          required: true
 *                          items:
 *                              type: object
 *                              properties:
 *                                  _id:
 *                                      type: string
 *                                      description: value of the type
 *                                  count:
 *                                      type: integer
 *                                      description: count of data
 *                      ageGroup:
 *                          type: array
 *                          description: Contains data by age group
 *                          required: true
 *                          items:
 *                              type: object
 *                              properties:
 *                                  _id:
 *                                      type: string
 *                                      description: value of the type
 *                                  count:
 *                                      type: integer
 *                                      description: count of data
 *                      byGender:
 *                          type: array
 *                          description: Contains data by gender
 *                          required: true
 *                          items:
 *                              type: object
 *                              properties:
 *                                  _id:
 *                                      type: string
 *                                      description: value of the type
 *                                  count:
 *                                      type: integer
 *                                      description: count of data
 *                      byBloodGroup:
 *                          type: array
 *                          description: Contains data by blood group type
 *                          required: true
 *                          items:
 *                              type: object
 *                              properties:
 *                                  _id:
 *                                      type: string
 *                                      description: value of the type
 *                                  count:
 *                                      type: integer
 *                                      description: count of data
 *                      message:
 *                          type: string
 *                          description: Success or failure message
 *                          required: true
 *       500:
 *         description: Status of adding the contributor to the team
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether the member has been added or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message regarding the adding of new team member
 *                          required: true
 */
router.post("/getGraphsData", verify_token, user_controller.getGraphsData);

/**
 * @swagger
 * /user/getUserDetails:
 *   post:
 *     tags: ['user']
 *     description: Get user details of user who is logged in
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: success
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: User details fetched successfully
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message regarding the user details fetch status
 *                          required: true
 *                      data:
 *                          type: object
 *                          description: User data
 *                          required: true
 *                          $ref: '#/components/schemas/user'
 *
 *       500:
 *         description: Status of retrieving user details
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether the user details have been fetched or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error Message fetching of user details
 *                          required: true
 */
router.post("/getUserDetails", verify_token, user_controller.getUserDetails);
module.exports = router;
