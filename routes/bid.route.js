const express = require("express");
const bid_controller = require("../controllers/bid.controller");
var verify_token = require("./verify.token");

const router = express.Router();

/**
 * @swagger
 * components:
 *  securitySchemes:
 *      AccessToken:
 *          type: apiKey
 *          in: header
 *          name: x-access-token
 *  schemas:
 *    bid:
 *      properties:
 *       _id:
 *          type: string
 *          description: The unique id for the bid
 *       bidTitle:
 *          type: string
 *          description: The name of the bid in format of buyer_cancertype_uniqueid
 *       cancerType:
 *          type: string
 *          description: Type of the cancer
 *       recordsRequired:
 *          type: string
 *          description: Number of the records required by the buyer
 *       recordsAcquired:
 *          type: string
 *          description: Number of records bought so far
 *       bidStartDate:
 *          type: string
 *          format: date
 *          description: When the bid needs to be visible for the all the sellers
 *       bidExpiryDate:
 *          type: string
 *          format: date
 *          description: When the bid is expired and wont be shown to sellers
 *       amountPerRecord:
 *          type: number
 *          description: Amount buyer is ready to invest for all the records
 *       bidStatus:
 *          type: string
 *          description: Is the bid active or made inactive by the buyer
 *          enum: [ACTIVE, INACTIVE]
 *       teamId:
 *          type: string
 *          description: If record of a particular team is to be bought then this field is populated
 *       recordAccessStartDate:
 *          type: string
 *          format: date
 *          description: The date from which the record needs to be accessible buyer
 *       recordAccessExpiryDate:
 *          type: string
 *          format: date
 *          description: The date on which the access to the record will be revoked for buyer
 *       buyer:
 *          type: string
 *          description: The id of the user who is buying he record
 *       recordBought:
 *          type: array
 *          items:
 *              type: string
 *          description: record ids of the records that have been purchased under the bid
 *       recordAccepted:
 *          type: array
 *          items:
 *              type: string
 *          description: list of all the records for which teams have accepted
 *    bidTransition:
 *      properties:
 *          _id:
 *              type: string
 *              description: Unique id given by the mongo itself
 *          bid:
 *              type: string
 *              description: contains id in reference to the bid the transaction is related to
 *          record:
 *              type: string
 *              description: contains id in reference to record for which the bid is related to
 *          buyer:
 *              type: string
 *              description: contains id in reference to buyer to which the bid is related to
 *          seller:
 *              type: string
 *              description: contains id in reference to seller who is either rejecting or accepting the bid
 *          team:
 *              type: string
 *              description: contains id in reference to team to which seller belongs to
 *          comment:
 *              type: string
 *              description: comment by the seller when the seller rejects the bid
 *          status:
 *              type: string
 *              description: bid is accepted (A) or rejected (R) by the seller
 */
/**
 * @swagger
 * /bid/createBid:
 *   post:
 *     tags: ['bids']
 *     description: Create a bid
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      name: Medical Record
 *      description: Medical Record details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      cancerType:
 *                          type: string
 *                          description: Type of the cancer
 *                      recordsRequired:
 *                          type: number
 *                          description: Number of the records required by the buyer
 *                      bidStartDate:
 *                          type: string
 *                          format: date
 *                          description: When the bid needs to be visible for the all the sellers
 *                      bidExpiryDate:
 *                          type: string
 *                          format: date
 *                          description: When the bid is expired and wont be shown to sellers
 *                      amountPerRecord:
 *                          type: number
 *                          description: Amount buyer is ready to invest per records
 *                      bidStatus:
 *                          type: string
 *                          description: Is the bid active or made inactive by the buyer
 *                          enum: [ACTIVE, INACTIVE]
 *                      recordAccessStartDate:
 *                          type: string
 *                          format: date
 *                          description: The date from which the record needs to be accessible buyer
 *                      recordAccessExpiryDate:
 *                          type: string
 *                          format: date
 *                          description: The date on which the access to the record will be revoked for buyer
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful created a bid
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid was successfully created or not
 *                          required: true
 *                      data:
 *                          type : object
 *                          $ref: '#/components/schemas/bid'
 *                          description: bid saved in the db
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successful creation of bid
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid was successfully created or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message for failure of creation of bid
 *                          required: true
 */
router.post("/createBid", verify_token, bid_controller.createBid);
/**
 * @swagger
 * /bid/getBuyersBids:
 *   post:
 *     tags: ['bids']
 *     description: Listing all bids for Buyer
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful retrieved all bids
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid was successfully retrieved or not
 *                          required: true
 *                      data:
 *                          type : array
 *                          items :
 *                              type: object
 *                              $ref: '#/components/schemas/bid'
 *                          description: list of bids retrieved from the db
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successfully retrieved list of bids
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bids were successfully retrieved or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message for failure of listing of bid
 *                          required: true
 */

router.post("/getBuyersBids", verify_token, bid_controller.getBuyersBids);

/**
 * @swagger
 * /bid/getSellersBids:
 *   post:
 *     tags: ['bids']
 *     description: Listing all bids for Seller
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     security:
 *      - AccessToken: []
 *     requestBody:
 *      name: Medical Record
 *      description: Medical Record details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      status:
 *                          type: number
 *                          description: status can be 1 - Incoming, 2 - Pendinf, 3 - Accepted
 *     responses:
 *       200:
 *         description: Successful retrieved all bids
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid was successfully retrieved or not
 *                          required: true
 *                      data:
 *                          type : array
 *                          items :
 *                              type: object
 *                              properties:
 *                                  teamName:
 *                                      type: string
 *                                      description: The name of the team whose record the bid is related to
 *                                  bidName:
 *                                      type: string
 *                                      description: The name of the bid
 *                                  bidExpiry:
 *                                      type: string
 *                                      description: The expiry date of the bid
 *                                  bidAmountPerRecord:
 *                                      type: string
 *                                      description: The amount per record given in bid
 *                                  bidPersonalEarning:
 *                                      type: string
 *                                      description: The amount that the user will get in the bid
 *                                  recordId:
 *                                      type: string
 *                                      description: The record associated with the bid
 *                                  teamId:
 *                                      type: string
 *                                      description: The id of the team associated with the bid
 *                                  bidId:
 *                                      type: string
 *                                      description: The id of the bid
 *                                  bidBuyer:
 *                                      type: object
 *                                      $ref: '#/components/schemas/user'
 *                          description: list of bids retrieved from the db
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successfully retrieved list of bids
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bids were successfully retrieved or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message for failure of listing of bid
 *                          required: true
 */
router.post("/getSellersBids", verify_token, bid_controller.getSellerBids);

/**
 * @swagger
 * /bid/acceptBid:
 *   post:
 *     tags: ['bids']
 *     description: Accept a bid by the seller
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     security:
 *      - AccessToken: []
 *     requestBody:
 *      name: Details of bid
 *      description: Bid details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      teamId:
 *                          type: string
 *                          required: true
 *                      recordId:
 *                          type: string
 *                          required: true
 *                      buyerId:
 *                          type: string
 *                          required: true
 *                      bidId:
 *                          type: string
 *                          required: true
 *     responses:
 *       200:
 *         description: Accepting a particular bid
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid was successfully accepted or not
 *                      message:
 *                          type: string
 *                          description: Message regarding Whether bid was successfully accepted or not
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid was successfully accepted or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message regarding Whether bid was successfully accepted or not
 *                          required: true
 */
router.post("/acceptBid", verify_token, bid_controller.acceptBid);

/**
 * @swagger
 * /bid/buyerAcceptBids:
 *   post:
 *     tags: ['bids']
 *     description: Accept a bid by the buyer
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     security:
 *      - AccessToken: []
 *     requestBody:
 *      name: Details of bid
 *      description: Bid details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      bidId:
 *                          type: string
 *                          required: true
 *                      cardNumber:
 *                          type: string
 *                          required: true
 *                      cvc:
 *                          type: string
 *                          required: true
 *                      expiryDate:
 *                          type: string
 *                          format: date
 *                          required: true
 *                      recordIdList:
 *                          type: array
 *                          required: true
 *                          items:
 *                              type: string
 *     responses:
 *       200:
 *         description: Acceptance bids by a buyer
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid was successfully accepted or not
 *                      message:
 *                          type: string
 *                          description: Message regarding Whether bid was successfully accepted or not
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid was successfully accepted or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message regarding Whether bid was successfully accepted or not
 *                          required: true
 */
router.post("/buyerAcceptBids", verify_token, bid_controller.buyerAcceptBids);

/**
 * @swagger
 * /bid/getBuyersMaturedBids:
 *   post:
 *     tags: ['bids']
 *     description: Get all the matured bids of the buyer
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Expired bids of the buyer
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid was successfully retrieved or not
 *                      message:
 *                          type: string
 *                          description: Message regarding Whether bid was successfully retrieved or not
 *                          required: true
 *                      data:
 *                          type: array
 *                          description: list of all the bids matured
 *                          required: true
 *                          items:
 *                              type: object
 *                              properties:
 *                                  bidName:
 *                                      type: string
 *                                  numberOfRecordsAcquired:
 *                                      type: number
 *                                  numberOfRecordsRequired:
 *                                      type: number
 *                                  amountPerRecord:
 *                                      type: number
 *                                  recordAccessStartDate:
 *                                      type: date
 *                                  recordAccessExpiryDate:
 *                                      type: date
 *                                  cancerType:
 *                                      type: string
 *                                  recordId:
 *                                      type: string
 *                                  team:
 *                                      type: object
 *                                      $ref: '#/components/schemas/team'
 *                                  patient:
 *                                      type: string
 *                                      $ref: '#/components/schemas/user'
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid was successfully accepted or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message regarding Whether bid was successfully accepted or not
 *                          required: true
 */
router.post(
  "/getBuyersMaturedBids",
  verify_token,
  bid_controller.getBuyersMaturedBids
);

/**
 * @swagger
 * /bid/updateBidStatus:
 *   post:
 *     tags: ['bids']
 *     description: Update the status of a bid to INACTIVE
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      description: Bid Id
 *      name: Bid details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      bidId:
 *                          description: Id of the bid being made inactive
 *                          type: string
 *                          required: true
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Status of the bid is changed to INACTIVE
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid status was successfully changed or not
 *                      message:
 *                          type: string
 *                          description: Message stating whether bid status was updated or not
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid status was successfully updated or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message whether bid status was successfully updated or not
 *                          required: true
 */
router.post("/updateBidStatus", verify_token, bid_controller.updateBidStatus);

/**
 * @swagger
 * /bid/editBid:
 *   post:
 *     tags: ['bids']
 *     description: Update the bid of a buyer with the details given
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      description: Bid details
 *      name: Bid
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      cancerType:
 *                          type: string
 *                          description: Type of the cancer
 *                      recordsRequired:
 *                          type: number
 *                          description: Number of the records required by the buyer
 *                      bidStartDate:
 *                          type: string
 *                          format: date
 *                          description: When the bid needs to be visible for the all the sellers
 *                      bidExpiryDate:
 *                          type: string
 *                          format: date
 *                          description: When the bid is expired and wont be shown to sellers
 *                      amountPerRecord:
 *                          type: number
 *                          description: Amount buyer is ready to invest per records
 *                      bidStatus:
 *                          type: string
 *                          description: Is the bid active or made inactive by the buyer
 *                          enum: [ACTIVE, INACTIVE]
 *                      recordAccessStartDate:
 *                          type: string
 *                          format: date
 *                          description: The date from which the record needs to be accessible buyer
 *                      recordAccessExpiryDate:
 *                          type: string
 *                          format: date
 *                          description: The date on which the access to the record will be revoked for buyer
 *                      bidId:
 *                          type: string
 *                          format: string
 *                          description: The id of the bid being updated
 *
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Status of the bid is changed to INACTIVE
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid status was successfully updated or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Success message whether bid status was successfully updated or not
 *                          required: true
 *
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid status was successfully updated or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message whether bid status was successfully updated or not
 *                          required: true
 */
router.post("/editBid", verify_token, bid_controller.editBid);

/**
 * @swagger
 * /bid/createSingleBid:
 *   post:
 *     tags: ['bids']
 *     description: Create a bid
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      name: Bid
 *      description: Bid details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      recordId:
 *                          type: string
 *                          description: Id of the single record to be purchased
 *                      bidStartDate:
 *                          type: string
 *                          format: date
 *                          description: When the bid needs to be visible for the all the sellers
 *                      bidExpiryDate:
 *                          type: string
 *                          format: date
 *                          description: When the bid is expired and wont be shown to sellers
 *                      amountPerRecord:
 *                          type: number
 *                          description: Amount buyer is ready to invest per records
 *                      bidStatus:
 *                          type: string
 *                          description: Is the bid active or made inactive by the buyer
 *                          enum: [ACTIVE, INACTIVE]
 *                      recordAccessStartDate:
 *                          type: string
 *                          format: date
 *                          description: The date from which the record needs to be accessible buyer
 *                      recordAccessExpiryDate:
 *                          type: string
 *                          format: date
 *                          description: The date on which the access to the record will be revoked for buyer
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful created a bid
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid was successfully created or not
 *                          required: true
 *                      data:
 *                          type : object
 *                          $ref: '#/components/schemas/bid'
 *                          description: bid saved in the db
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successful creation of bid
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether bid was successfully created or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message for failure of creation of bid
 *                          required: true
 */
router.post("/createSingleBid", verify_token, bid_controller.createSingleBid);
module.exports = router;
