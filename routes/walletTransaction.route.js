const express = require("express");
const walletTrasactionController = require("../controllers/walletTransaction.controller");
var verify_token = require("./verify.token");

const router = express.Router();

/**
 * @swagger
 * components:
 *  securitySchemes:
 *      AccessToken:
 *          type: apiKey
 *          in: header
 *          name: x-access-token
 *  schemas:
 *    walletTransaction:
 *      properties:
 *          _id:
 *              type: string
 *              description: Unique id for the transaction
 *          userId:
 *              type: string
 *              description: contains id of the user whose wallet transaction we are doing
 *          accountHolderName:
 *              type: string
 *              description: Account holder name of acoount to which the amount needs to be transferred
 *          amount:
 *              type: number
 *              description: Amount to be transferred from wallet to user's account
 *          accountNumber:
 *              type: string
 *              description: Account number to which the amount needs to be transferred
 *          bsbNumber:
 *              type: string
 *              description: BSB number of acoount to which the amount needs to be transferred
 *          confirmationCode:
 *              type: string
 *              description: Verification code for the transaction
 *          verified:
 *              type: string
 *              description: Is the code verfication code cross checked
 *          transferred:
 *              type: string
 *              description: Is the transaction done and successful
 *          transferredDate:
 *              type: Date
 *              description: Date when the actual transaction happned post verifying the code
 */

/**
 * @swagger
 * /wallet/getVerificationCode:
 *   post:
 *     tags: ['wallet']
 *     description: Get a verificaton code for the transaction
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      name: Wallet Transaction
 *      description: Wallet Transaction details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      accountNumber:
 *                          type: string
 *                          description: Account number to which user wants to transfer
 *                      accountHolderName:
 *                          type: string
 *                          description: Account Holder name of account to which user wants to transfer
 *                      amount:
 *                          type: string
 *                          description: Amount that the user wants to transfer
 *                      bsbNumber:
 *                          type: number
 *                          description: bsb number of the account to which user wants to transfer
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful sent the verification code
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether verification code was successfully sent or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successfully sending the mail with verification code to user registered mail id
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether verification code was successfully sent or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message for failure to send verification code
 *                          required: true
 */
router.post(
  "/getVerificationCode",
  verify_token,
  walletTrasactionController.sendCode
);

/**
 * @swagger
 * /wallet/transferMoney:
 *   post:
 *     tags: ['wallet']
 *     description: Transfer money to the users account from user's wallet by verifying code
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      name: Wallet Transaction
 *      description: Wallet Transaction details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      accountNumber:
 *                          type: string
 *                          description: Account number to which user wants to transfer
 *                      accountHolderName:
 *                          type: string
 *                          description: Account Holder name of account to which user wants to transfer
 *                      amount:
 *                          type: string
 *                          description: Amount that the user wants to transfer
 *                      bsbNumber:
 *                          type: number
 *                          description: bsb number of the account to which user wants to transfer
 *                      verificationCode:
 *                          type: string
 *                          description: verification code entered by the user
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful transfer of amount to user account
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether transfer of amount to user account was successfull or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: message regarding transfer of amount to user account
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether transfer of amount to user account was successfull or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error message regarding transfer of amount to user account
 *                          required: true
 */
router.post(
  "/transferMoney",
  verify_token,
  walletTrasactionController.transferMoney
);
module.exports = router;
