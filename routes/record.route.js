const express = require("express");
const record_controller = require("../controllers/record.controller");
var verify_token = require("./verify.token");

const router = express.Router();

/**
 * @swagger
 * components:
 *  securitySchemes:
 *      AccessToken:
 *          type: apiKey
 *          in: header
 *          name: x-access-token
 *  schemas:
 *    user:
 *      properties:
 *       _id:
 *          type: string
 *          description: the unique id of the user in the db
 *       name:
 *          type: string
 *          description: Name of the user
 *       email:
 *          type: string
 *          description: Email of the user
 *       user_type:
 *          type: string
 *          description: Type of the user lie patient, doctor
 *       password:
 *          type: string
 *          description: Encrypted password
 *      isVerified:
 *          type: booelan
 *          description: Email verification status
 *      resetPasswordToken:
 *          type: string
 *          description: token for resetting password
 *      resetPasswordExpires:
 *          type: date
 *          description: Current token expiry date
 *      _v:
 *        type: string
 *        description: version generated in the db
 *    team:
 *      properties:
 *       _id:
 *          type: string
 *          description: the unique id of the team in the db
 *       name:
 *          type: string
 *          description: Name of the team which is in format ContributorsList_PATIENT-EMAIL
 *       admin:
 *          type: string
 *          description: id of the patient
 *       members:
 *          type: array
 *          description: Array of ids of members in team who can typically be the doctors, hospitals, laboratories
 *          items:
 *            type: string
 */

/**
 * @swagger
 * /record/addRecord:
 *   post:
 *     tags: ['record']
 *     description: Add a record
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *      name: Medical Record
 *      description: Medical Record details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      patientId:
 *                          type: string
 *                          required: true
 *                          description: Id of the patient to whom the record belongs.
 *                      teamId:
 *                          type: string
 *                          required: true
 *                          description: Id of the team to whom the record belongs.
 *                      cancer_type:
 *                          type: string
 *                          required: false
 *                          description: Type of cancer
 *                      age_at_diagnosis:
 *                          type: string
 *                          required: false
 *                          description: Age of patient at the time of diagnosis.
 *                      cs_tumor_size:
 *                          type: string
 *                          required: false
 *                          description: Size of tumor.
 *                      gender:
 *                          type: string
 *                          required: false
 *                          description: Patient's gender.
 *                      year_of_birth:
 *                           type: string
 *                           required: false
 *                           description: Patient's year of birth.
 *                      date_of_diagnosis:
 *                           type: string
 *                           format: date
 *                           required: false
 *                           description: Date of diagnosis.
 *                      cweight:
 *                           type: number
 *                           required: false
 *                           description: Patient's weight.
 *                      bpressure:
 *                           type: number
 *                           required: false
 *                           description: Blood Pressure of Patient.
 *                      blood_group:
 *                           type: string
 *                           required: false
 *                           description: Blood group of Patient.
 *                      visit_date:
 *                           type: string
 *                           format: date
 *                           required: false
 *                           description: Visit date of Patient.
 *                      outcome:
 *                           type: string
 *                           required: false
 *                           description: Result's outcome.
 *                      outcome_date:
 *                          type: string
 *                          format: date
 *                          required: false
 *                          description: Date of result'outcome.
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Status of the adding the record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Status of the adding the record
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successful addition of record message
 *                          required: true
 *       500:
 *         description: Status of the adding the record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Status of the adding the record
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error behind addition of record message
 *                          required: true
 */
router.post("/addRecord", verify_token, record_controller.addRecord);
/**
 * @swagger
 * /record/my-record:
 *   get:
 *     tags: ['record']
 *     description: Retrieve record of a user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userId
 *         description: Id of the user.
 *         in: query
 *         required: true
 *         schema:
 *          type: string
 *     responses:
 *       200:
 *         description: Status of retrieving the user medical record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether access token is valid or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message with respect to the outcome of authentication. Can contain success or respective error message.
 *                          required: true
 *                      data:
 *                          type: array
 *                          description: Contains the medical record data
 *                          required: true
 *                          items:
 *                              type: object
 *                              properties:
 *                                  cs_tumor_size:
 *                                      type: string
 *                                      description: Tumor size
 *                                  year_of_birth:
 *                                      type: string
 *                                      description: Year of birth of user
 *                                  gender:
 *                                      type: string
 *                                      description: Gender of user
 *                                  cancer_type:
 *                                      type: string
 *                                      description: Type of cancer
 *                                  price:
 *                                      type: string
 *                                      description: Price for buying record
 *                                  user:
 *                                    type: object
 *                                    $ref: '#/components/schemas/user'
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether retreival of medical record is successful or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error behind failure of retrieval of medical records
 *                          required: true
 */
router.get("/my-records", record_controller.listMyRecords);
/**
 * @swagger
 * /record/deleteRecord:
 *   post:
 *     tags: ['record']
 *     description: Delete record of a user
 *     produces:
 *       - application/json
 *     requestBody:
 *      description: Medical record and user details
 *      name: Record Details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      id:
 *                          description: Id of the record being deleted
 *                          type: string
 *                          required: true
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful deletion
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Successful deletion
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successful deletion message
 *                          required: true
 *       500:
 *         description: Gives the error message for deletion
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Failure of deletion
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error behind failure of deletion of message
 *                          required: true
 */
router.post("/deleteRecord", verify_token, record_controller.deleteRecord);
/**
 * @swagger
 * /record/updateRecord:
 *   post:
 *     tags: ['record']
 *     description: Update a record
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *      name: Medical Record
 *      description: Medical Record details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      data:
 *                          type: object
 *                          required: true
 *                          properties:
 *                              _id:
 *                                  type: string
 *                                  required: true
 *                                  description: id of the record.
 *                              user:
 *                                  type: object
 *                                  $ref: '#/components/schemas/user'
 *                              doctor:
 *                                  type: object
 *                                  $ref: '#/components/schemas/user'
 *                              price:
 *                                  type: string
 *                                  required: false
 *                                  description: Price for buying the record.
 *                              cancer_type:
 *                                  type: string
 *                                  required: false
 *                                  description: Type of cancer
 *                              age_at_diagnosis:
 *                                  type: string
 *                                  required: false
 *                                  description: Age of patient at the time of diagnosis.
 *                              cs_tumor_size:
 *                                  type: string
 *                                  required: false
 *                                  description: Size of tumor.
 *                              gender:
 *                                  type: string
 *                                  required: false
 *                                  description: Patient's gender.
 *                              year_of_birth:
 *                                  type: string
 *                                  required: false
 *                                  description: Patient's year of birth.
 *                              data:
 *                                  type: string
 *                                  required: false
 *                                  description: Additional data.
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Success status of updating record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Successfully updated records
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Success message for updating record
 *                          required: true
 *       500:
 *         description: Failure of updating record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Update record failed
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message for failure of updating record
 *                          required: true
 */
router.post("/updateRecord", verify_token, record_controller.updateRecord);
/**
 * @swagger
 * /record/listRecords:
 *   post:
 *     tags: ['record']
 *     description: Pagination for all records
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      name: From and to of records
 *      description: Provides records from particular indices
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      from:
 *                          type: integer
 *                          description: The starting index
 *                          required: true
 *                      size:
 *                          type: integer
 *                          description: The number of records required
 *                          required: true
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful status of retrieving the all the medical record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Successfully retrieved all records
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message stating successfully retrival of all records
 *                          required: true
 *                      data:
 *                          type: array
 *                          description: Contains the medical record data
 *                          required: true
 *                          items:
 *                              type: object
 *                              properties:
 *                                  user:
 *                                      type: object
 *                                      $ref: '#/components/schemas/user'
 *                                  doctor:
 *                                      type: object
 *                                      $ref: '#/components/schemas/user'
 *                                  name:
 *                                      type: string
 *                                      description: Name of the patient
 *                                  cs_tumor_size:
 *                                      type: string
 *                                      description: Tumor size
 *                                  year_of_birth:
 *                                      type: string
 *                                      description: Year of birth of user
 *                                  gender:
 *                                      type: string
 *                                      description: Gender of user
 *                                  cancer_type:
 *                                      type: string
 *                                      description: Type of cancer
 *                                  price:
 *                                      type: string
 *                                      description: Price for buying record
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Failure to retrieve data
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message for failing to retreive records
 *                          required: true
 */
router.post("/listRecords", verify_token, record_controller.listRecords);
/**
 * @swagger
 * /record/listOwnerRecords:
 *   post:
 *     tags: ['record']
 *     description: Pagination for a particular user record
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      name: From and to of all user records
 *      description: Provide user records from particular index
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      from:
 *                          type: integer
 *                          description: The starting index
 *                          required: true
 *                      size:
 *                          type: integer
 *                          description: The number of records required
 *                          required: true
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful status of retrieving the all medical records of a user
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Successfully retrieved all records for a user
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message stating successfully retrival of all records for a user
 *                          required: true
 *                      data:
 *                          type: array
 *                          description: Contains the medical record data for a user
 *                          required: true
 *                          items:
 *                              type: object
 *                              properties:
 *                                  user:
 *                                      type: object
 *                                      $ref: '#/components/schemas/user'
 *                                  cs_tumor_size:
 *                                      type: string
 *                                      description: Tumor size
 *                                  year_of_birth:
 *                                      type: string
 *                                      description: Year of birth of user
 *                                  gender:
 *                                      type: string
 *                                      description: Gender of user
 *                                  cancer_type:
 *                                      type: string
 *                                      description: Type of cancer
 *                                  price:
 *                                      type: string
 *                                      description: Price for buying record
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Failure to retrieve data
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message for failing to retreive records
 *                          required: true
 */
router.post(
  "/listOwnerRecords",
  verify_token,
  record_controller.listOwnerRecords
);
/**
 * @swagger
 * /record/viewRecord:
 *   post:
 *     tags: ['record']
 *     description: Retrieve a particular record
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      name: Medical record id
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      id:
 *                          type: string
 *                          description: Id of medical record
 *                          required: true
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Status of retrieving the user medical record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether access token is valid or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message with respect to the outcome of authentication. Can contain success or respective error message.
 *                          required: true
 *                      data:
 *                          type: object
 *                          description: Contains the medical record data
 *                          required: true
 *                          properties:
 *                              id:
 *                                  type: string
 *                                  description: Id of te medical record
 *                              name:
 *                                  type: string
 *                                  description: Name of the patient
 *                              cs_tumor_size:
 *                                  type: string
 *                                  description: Tumor size
 *                              year_of_birth:
 *                                  type: string
 *                                  description: Year of birth of user
 *                              gender:
 *                                  type: string
 *                                  description: Gender of user
 *                              cancer_type:
 *                                  type: string
 *                                  description: Type of cancer
 *                              price:
 *                                  type: string
 *                                  description: Price for buying record
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether access token is valid or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message with respect the outcome of authentication. Can contain success or respective error message.
 *                          required: true
 */
router.post("/viewRecord", verify_token, record_controller.viewRecord);
/**
 * @swagger
 * /record/changeOwnership:
 *   post:
 *     tags: ['record']
 *     description: Transfer ownership of a record to another doctor
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      name: User details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      medicalrecordId:
 *                          type: string
 *                          description: Id of medical record
 *                          required: true
 *                      newOwnerMail:
 *                          type: string
 *                          description: Mail id of user to whom the record needs to be transferred
 *                          required: true
 *                      newOwnerId:
 *                          type: string
 *                          description: Id of user to whom the record needs to be transferred
 *                          required: true
 *                      currentUserId:
 *                          type: string
 *                          description: Id of of the current owner
 *                          required: true
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful transfer
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether transfer was successfull or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successful transfer of record message
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether transfer was successfull or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error meesage for failure of transfer of record
 *                          required: true
 */
router.post(
  "/changeOwnership",
  verify_token,
  record_controller.changeOwnership
);

/**
 * @swagger
 * /record/getTeamRecord:
 *   post:
 *     tags: ['team']
 *     description: Get medical record by team Id
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      name: Medical record
 *      required: false
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      teamId:
 *                          type: string
 *                          description: Id of team (for patient access token will contain team id)
 *                          required: false
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful retrieval of medical record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether retrieval of medical record was successful or not
 *                          required: true
 *                      data:
 *                          type: object
 *                          description: Medical Record details
 *                          required: true
 *                          properties:
 *                                  name:
 *                                      type: string
 *                                      description: Name of the patient
 *                                  cs_tumor_size:
 *                                      type: string
 *                                      description: Tumor size
 *                                  year_of_birth:
 *                                      type: string
 *                                      description: Year of birth of user
 *                                  gender:
 *                                      type: string
 *                                      description: Gender of user
 *                                  cancer_type:
 *                                      type: string
 *                                      description: Type of cancer
 *                                  price:
 *                                      type: string
 *                                      description: Price for buying record
 *                      message:
 *                          type: string
 *                          description: Successful transfer of record message
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether retrieval of medical record was successful or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error meesage for failure of retrieval of medical record
 *                          required: true
 */
router.post("/getTeamRecord", verify_token, record_controller.getTeamRecord);

/**
 * @swagger
 * /record/getRecordCountForCancerType:
 *   post:
 *     tags: ['record']
 *     description: Get count of medical record for a cancer type
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      name: Cancer types
 *      required: false
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      cancerType:
 *                          type: string
 *                          description: Type of the cancer whose count is required
 *                          required: true
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful retrieval of count for a given cancer type
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether retrieval of count for a given cancer type was successful or not
 *                          required: true
 *                      count:
 *                          type: number
 *                          description: Count of medical records for a given cancer type
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successful retrieval of count for a given cancer type
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether retrieval of count for a given cancer type was successful or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error meesage for failure of retrieval of count for a given cancer type
 *                          required: true
 */
router.post(
  "/getRecordCountForCancerType",
  verify_token,
  record_controller.getCountforCancerType
);

/**
 * @swagger
 * /record/getAllCancerTypes:
 *   post:
 *     tags: ['record']
 *     description: Get all cancer types present in the application
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful retrieval of all cancer types in the application
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether retrieval of count for a given cancer type was successful or not
 *                          required: true
 *                      data:
 *                          type: array
 *                          description: List of all the cancer types present
 *                          items:
 *                            type: string
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successful retrieval of count for a given cancer type
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether retrieval of count for a given cancer type was successful or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error meesage for failure of retrieval of count for a given cancer type
 *                          required: true
 */
router.post(
  "/getAllCancerTypes",
  verify_token,
  record_controller.getAllCancerTypes
);

/**
 * @swagger
 * /record/getRecordsForBid:
 *   post:
 *     tags: ['record']
 *     description: Get records for a bid
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      name: Body request
 *      description: Request body
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      bidId:
 *                          type: string
 *                          description: The starting index
 *                          required: true
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful status of retrieving the all medical records of a user
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Successfully retrieved all records for a user
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message stating successfully retrival of all records for a user
 *                          required: true
 *                      data:
 *                          type: array
 *                          description: Contains the medical record data for a user
 *                          required: true
 *                          items:
 *                              type: object
 *                              properties:
 *                                  user:
 *                                      type: object
 *                                      $ref: '#/components/schemas/user'
 *                                  cs_tumor_size:
 *                                      type: string
 *                                      description: Tumor size
 *                                  year_of_birth:
 *                                      type: string
 *                                      description: Year of birth of user
 *                                  gender:
 *                                      type: string
 *                                      description: Gender of user
 *                                  cancer_type:
 *                                      type: string
 *                                      description: Type of cancer
 *                                  price:
 *                                      type: string
 *                                      description: Price for buying record
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Failure to retrieve data
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message for failing to retreive records
 *                          required: true
 */
router.post(
  "/getRecordsForBid",
  verify_token,
  record_controller.getRecordsForBid
);

/**
 * @swagger
 * /record/getBuyerswithPermission:
 *   post:
 *     tags: ['record']
 *     description: Pagination for a particular user record
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Status of the authentication of user
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether access token is valid or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message with respect the outcome of authentication.
 *                          required: true
 *                      data:
 *                          type: array
 *                          description: List of all the buyers
 *                          items:
 *                            type: string
 *                          required: true
 *       500:
 *         description: Status of the authentication of user
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether access token is valid or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Erro message with respect the outcome of authentication.
 *                          required: true
 */
router.post(
  "/getBuyerswithPermission",
  verify_token,
  record_controller.getBuyerswithPermission
);

/**
 * @swagger
 * /record/addFileToRecord:
 *   post:
 *     tags: ['record']
 *     description: Upload multimedia file to the record
 *     produces:
 *       - application/json
 *     consumes:
 *      - multipart/form-data
 *     security:
 *      - AccessToken: []
 *     requestBody:
 *      content:
 *        multipart/form-data:
 *          schema:
 *            type: object
 *            properties:
 *              fileName:
 *                type: string
 *                format: binary
 *              recordId:
 *                type: string
 *              patientId:
 *                type: string
 *              teamId:
 *                type: string
 *              fileType:
 *                type: string
 *     responses:
 *       200:
 *         description: Status of the upload of the file
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether file is uploaded successfully
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Message with respect to upload of the file.
 *                          required: true
 *       500:
 *         description: Status of the upload of file
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether file is uploaded successfully
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error Message with respect to upload of the file.
 *                          required: true
 */
router.post(
  "/addFileToRecord",
  verify_token,
  record_controller.uploadFileToRecord
);

/**
 * @swagger
 * /record/downloadFileFromRecord:
 *   post:
 *     tags: ['record']
 *     description: Download multimedia file from the record
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     security:
 *      - AccessToken: []
 *     requestBody:
 *      name: Body request
 *      description: Request body
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      teamId:
 *                          type: string
 *                          required: true
 *                      recordId:
 *                          type: string
 *                          required: true
 *                      fileId:
 *                          type: string
 *                          required: true
 *     responses:
 *       200:
 *         description: Status of the Download of the file
 *         content:
 *          image/png:
 *              schema:
 *                type: string
 *                format: binary
 *          image/jpg:
 *              schema:
 *                type: string
 *                format: binary
 *          application/pdf:
 *              schema:
 *                type: string
 *                format: binary
 *          text/plain:
 *              schema:
 *                type: string
 *       500:
 *         description: Status of the download of file
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether file is downloaded successfully
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error Message with respect to download of the file.
 *                          required: true
 */
router.post(
  "/downloadFileFromRecord",
  verify_token,
  record_controller.downloadFileFromRecord
);

module.exports = router;
