const express = require("express");
const subrecord_controller = require("../controllers/subrecord.controller");
const verify_token = require("./verify.token");

const router = express.Router();
/**
 * @swagger
 *
 * definitions:
 *   SubRecord:
 *     type: object
 *     required:
 *       - patient
 *       - team
 *       - data
 *       - doctor
 *     properties:
 *       patient:
 *         type: string
 *       _id:
 *         type: string
 *       team:
 *         type: string
 *       data:
 *         type: string
 *       doctor:
 *         type: string
 *       createdAt:
 *         type: string
 *         format: date-time
 *       updatedAt:
 *         type: string
 *         format: date-time
 */

/**
 * @swagger
 * /subrecord/add:
 *   post:
 *     tags: ['subrecord']
 *     description: Add a subrecord
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *      name: Sub Medical Record
 *      description: Sub Medical Record details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      patientId:
 *                          type: string
 *                          required: true
 *                          description: Id of the patient to whom the record belongs.
 *                      teamId:
 *                          type: string
 *                          required: true
 *                          description: Id of the team to whom the record belongs.
 *                      data:
 *                          type: string
 *                          required: true
 *                          description: data of sub medical record in string format of '[{key:value},{key:value}]' format.
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Status of the adding the record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Status of the adding the record
 *                          required: true
 *                      data:
 *                          type : object
 *                          $ref: '#/definitions/SubRecord'
 *                          description: value of stored medical record returned
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successful addition of record message
 *                          required: true
 *       500:
 *         description: Status of the adding the record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Status of the adding the record
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error behind addition of record message
 *                          required: true
 */
router.post("/add", verify_token, subrecord_controller.add);
/**
 * @swagger
 * /subrecord/list:
 *   post:
 *     tags: ['subrecord']
 *     description: list subrecords
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *      name: List Sub Medical Records
 *      description: List Sub Medical Records
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                     teamId:
 *                          type: string
 *                          required: true
 *                          description: Id of the team to whom the record belongs.
 *                     from:
 *                          type: integer
 *                          description: From number for pagination
 *                     size:
 *                          type: integer
 *                          description: Size of the return list
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Status of the adding the record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Status of the adding the record
 *                          required: true
 *                      data:
 *                          type : array
 *                          items:
 *                              $ref: '#/definitions/SubRecord'
 *                          description: list of medical records
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successful addition of record message
 *                          required: true
 *       500:
 *         description: Status of the adding the record
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Status of the adding the record
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error behind addition of record message
 *                          required: true
 */
router.post("/list", verify_token, subrecord_controller.list);
module.exports = router;
