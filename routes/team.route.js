const express = require("express");
const team_controller = require("../controllers/team.controller");
const verify_token = require("./verify.token");

const router = express.Router();

/**
 * @swagger
 * components:
 *  securitySchemes:
 *      AccessToken:
 *          type: apiKey
 *          in: header
 *          name: x-access-token
 *  schemas:
 *    user:
 *      properties:
 *       _id:
 *          type: string
 *          description: the unique id of the user in the db
 *       name:
 *          type: string
 *          description: Name of the user
 *       email:
 *          type: string
 *          description: Email of the user
 *       user_type:
 *          type: string
 *          description: Type of the user lie patient, doctor
 *       password:
 *          type: string
 *          description: Encrypted password
 *      isVerified:
 *          type: booelan
 *          description: Email verification status
 *      resetPasswordToken:
 *          type: string
 *          description: token for resetting password
 *      resetPasswordExpires:
 *          type: date
 *          description: Current token expiry date
 *      _v:
 *        type: string
 *        description: version generated in the db
 *    team:
 *      properties:
 *       _id:
 *          type: string
 *          description: the unique id of the team in the db
 *       name:
 *          type: string
 *          description: Name of the team which is in format ContributorsList_PATIENT-EMAIL
 *       admin:
 *          type: object
 *          $ref: '#/components/schemas/user'
 *          description: id of the patient
 *       adminShare:
 *          type: number
 *          description: The profit share of the patient
 *       members:
 *          type: array
 *          description: Array of ids of members in team who can typically be the doctors, hospitals, laboratories
 *          items:
 *            type: object
 *            properties:
 *              user:
 *                  type: string
 *                  description: The member of the team
 *              share:
 *                  type: number
 *                  description: Profit share for the member
 *
 */

/**
 * @swagger
 * /team/getDoctorTeams:
 *   post:
 *     tags: ['team']
 *     description: Get teams by doctor Id
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Successful transfer
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether transfer was successfull or not
 *                          required: true
 *                      data:
 *                          type : array
 *                          items:
 *                              $ref: '#/components/schemas/team'
 *                          description: List of teams
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successful transfer of record message
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether transfer was successfull or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error meesage for failure of transfer of record
 *                          required: true
 */
router.post("/getDoctorTeams", verify_token, team_controller.getDoctorTeams);

/**
 * @swagger
 * /team/updateMemberShare:
 *   post:
 *     tags: ['team']
 *     description: Update team member share
 *     produces:
 *       - application/json
 *     consumes:
 *      - application/json
 *     requestBody:
 *      name: Members Details
 *      description: Members Details
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      memberId:
 *                          type: string
 *                          required: true
 *                          description: Id of the team member
 *                      share:
 *                          type: number
 *                          required: true
 *                          description: share percentage of member
 *     security:
 *      - AccessToken: []
 *     responses:
 *       200:
 *         description: Share updated successfully
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether transfer was successfull or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Successful transfer of share message
 *                          required: true
 *       500:
 *         description: Gives the error message
 *         content:
 *          application/json:
 *              schema:
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                          description: Whether transfer was successfull or not
 *                          required: true
 *                      message:
 *                          type: string
 *                          description: Error meesage for failure of transfer of record
 *                          required: true
 */
router.post(
    "/updateMemberShare",
    verify_token,
    team_controller.updateMemberShare
  );
module.exports = router;
