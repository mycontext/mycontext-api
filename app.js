const express = require("express");
const bodyParser = require("body-parser");
const userRoute = require("./routes/user.route");
const teamRoute = require("./routes/team.route");
const recordRoute = require("./routes/record.route");
const subRecordRoute = require("./routes/subrecord.route");
const bidRoute = require("./routes/bid.route");
const walletRoute = require("./routes/walletTransaction.route");
const mongoose = require("mongoose");
const cors = require("cors");
const helmet = require("helmet");
const swaggerUi = require("swagger-ui-express");
const swaggerSpec = require("./lib/swagger-jsdoc");
const logger = require("./utils/logger");
const httpLogger = require("./utils/httplogger");

const app = express();
const config = require("config");
const dbConfig = config.get("dbConfig");
const allowedOrigins = config.get("allowedOrigins");

app.use(httpLogger);
app.use(helmet());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(bodyParser.json());

mongoose.set("useFindAndModify", false);

mongoose.connect(
  dbConfig.host,
  {
    auth: {
      user: dbConfig.user,
      password: dbConfig.password,
    },

    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  function (err, client) {
    if (err) {
      logger.error(err);
      console.log(err);
    }
    logger.info("MongoDB connected!");
  }
);

mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.use(
  cors({
    origin: function (origin, callback) {
      if (!origin) {
        logger.error(`Error occured while accessing this url ${origin}`);
        return callback(null, true);
      }
      if (allowedOrigins.split(",").indexOf(origin) === -1) {
        if (origin.indexOf("chrome-extension") !== -1) {
          return callback(null, true);
        }

        var msg =
          "The CORS policy for this site does not " +
          "allow access from the specified Origin.";
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    },
  })
);

app.get("/", (req, res) => {
  res.send(
    "<h1 style='font-family:monospace;text-align:center;font-size:72px'><br/><br/><br/>🚀<br/>Bring it on! MyContext API is running. </h1>"
  );
});

app.use("/user", userRoute);
app.use("/record", recordRoute);
app.use("/subrecord", subRecordRoute);
app.use("/team", teamRoute);
app.use("/bid", bidRoute);
app.use("/wallet", walletRoute);

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));
const port = process.env.PORT || 9000;

app.listen(port, () => {
  logger.info(`MyContext API up and running on the ${port}`);
});
