const Record = require("../models/record.model");
const BuyRecords = require("../models/buy.model");
const Bid = require("../models/bid.model");
const Request = require("request");
const User = require("../models/user.model");
const Team = require("../models/team.model");
const apiConstructor = require("node-object-hash");
const Cryptr = require("cryptr");
const config = require("config");
const logger = require("../utils/logger");
const constant = require("../utils/constants");
const async = require("async");
const Busboy = require("busboy");
const mongoose = require("mongoose");
var Grid = require("gridfs-stream");
const { stream } = require("../utils/logger");
const nodemailer = require("nodemailer");
Grid.mongo = mongoose.mongo;

exports.getTeamRecord = function (req, res) {
  const teamId = req.body.teamId;

  Record.findOne({
    team: teamId,
  })
    .populate("doctor")
    .then((result) => {
      Team.findById(teamId)
        .populate("members.user admin")
        .then((team) => {
          return res.status(200).json({
            success: true,
            data: result,
            team: team,
            message: "Record fetched successfully from Team table",
          });
        })
        .catch((error) => {
          logger.error("error");

          return res.status(500).json({
            success: false,
            message: error.message,
          });
        });
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};

exports.addRecord = function (req, res) {
  const new_record = new Record({
    team: req.body.teamId,
    patient: req.body.patientId,
    doctor: req.body.userId,
    cancer_type: req.body.data.cancer_type,
    age_at_diagnosis: req.body.data.age_at_diagnosis,
    cs_tumor_size: req.body.data.cs_tumor_size,
    gender: req.body.data.gender,
    year_of_birth: req.body.data.year_of_birth,
    date_of_diagnosis: new Date(req.body.data.date_of_diagnosis),
    cweight: req.body.data.cweight,
    bpressure: req.body.data.bpressure,
    blood_group: req.body.data.blood_group,
    visit_date: new Date(req.body.data.visit_date),
    outcome: req.body.data.outcome,
    outcome_date: new Date(req.body.data.outcome_date),
  });

  const hasher = apiConstructor().hash;
  const hash = hasher(new_record);

  new_record
    .save()
    .then((result) => {
      var medicalRecord = {
        $class: "com.mycontext.MedicalRecord",
        medicalRecordId: new_record.id,
        hash: hash,
        owner: "resource:com.mycontext.Owner#" + req.body.patientId,
      };

      Request.post(
        {
          headers: {
            "content-type": "application/json",
          },
          url: config.get("blockchainUrl") + "api/com.mycontext.MedicalRecord",
          body: JSON.stringify(medicalRecord),
        },
        (error, response, body) => {
          if (error) {
            result.remove();

            return res.status(500).json({
              success: false,
              message: "Failed to save medical record.",
            });
          }

          return res.status(200).json({
            success: true,
            message: "Medical record added successfully.",
          });
        }
      );
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: "Failed to save medical record.",
      });
    });
};

exports.getRecordsForBid = function (req, res) {
  const bidId = req.body.bidId;
  const userId = req.body.userId;

  Bid.findOne({
    buyer: userId,
    _id: bidId,
  })
    .populate("recordBought")
    .then((bid) => {
      if (bid) {
        if (bid.recordAccessExpiryDate > new Date()) {
          if (bid.recordAccessStartDate < new Date()) {
            return res.status(200).json({
              success: true,
              data: bid.recordBought,
              message: "Record fetched successfully",
            });
          } else {
            return res.status(500).json({
              success: false,
              message: "Record access hasn't started",
            });
          }
        } else {
          return res.status(500).json({
            success: false,
            message: "Record access has expired",
          });
        }
      } else {
        return res.status(500).json({
          success: false,
          message: "No bids available",
        });
      }
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};

exports.updateRecord = function (req, res) {
  Record.findOneAndUpdate(
    {
      _id: req.body.data._id,
    },
    req.body.data
  )
    .then((result) => {
      return res.status(200).json({
        success: true,
        message: "Medical record updated successfully.",
      });
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: "Failed to update medical record.",
      });
    });
};

exports.listRecords = function (req, res) {
  const from = req.body.from;
  const size = req.body.size;

  Record.find({})
    .populate("patient doctor")
    .skip(from)
    .limit(size)
    .then((result) => {
      return res.status(200).json({
        success: true,
        data: result,
        message: "Medical records fetched successfully.",
      });
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: "Unable to fetch medical records.",
      });
    });
};

exports.listMyRecords = function (req, res) {
  const userId = req.query.userId;

  BuyRecords.find(
    {
      userId: userId,
    },
    ["recordId"]
  )
    .then((result) => {
      console.log(
        "result",
        result.map((x) => x.recordId)
      );
      Record.find(
        {
          _id: {
            $in: result.map((x) => x.recordId),
          },
        },
        ["cs_tumor_size", "year_of_birth", "gender", "cancer_type", "price"]
      )
        .populate("user")
        .then((ordinalRecords) => {
          return res.status(200).json({
            success: true,
            data: ordinalRecords,
            message: "Medical records fetched successfully.",
          });
        })
        .catch((error) => {
          logger.error("error");

          return res.status(500).json({
            success: false,
            message: "Unable to fetch medical records.",
          });
        });
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: "Unable to fetch medical records.",
      });
    });
};

exports.listOwnerRecords = function (req, res) {
  const from = req.body.from;
  const size = req.body.size;
  const userId = req.body.userId;

  const filter =
    "%7B%22where%22%3A%7B%22owner%22%3A%20%22resource%3Acom.mycontext.Owner%23" +
    userId +
    "%22%7D%7D";

  Request(
    config.get("blockchainUrl") +
      "api/com.mycontext.MedicalRecord?filter=" +
      filter,
    function (error, response, body) {
      if (body) {
        const records = JSON.parse(body);
        const ids = [];

        records.map((record) => {
          ids.push(record.medicalRecordId);
        });

        Record.find(
          {
            _id: {
              $in: ids,
            },
          },
          [
            "cs_tumor_size",
            "year_of_birth",
            "gender",
            "year_of_birth",
            "cancer_type",
            "price",
          ]
        )
          .populate("user")
          .skip(from)
          .limit(size)
          .then((result) => {
            return res.status(200).json({
              success: true,
              data: result,
              message: "Owner medical records fetched successfully.",
            });
          })
          .catch((error) => {
            logger.error("error");

            return res.status(500).json({
              success: false,
              message: "Unable to fetch medical records.",
            });
          });
      } else {
        return res.status(500).json({
          success: false,
          message: "Unable to load block chain.",
        });
      }
    }
  );
};

exports.viewRecord = function (req, res) {
  const id = req.body.id;

  Record.findById(id)
    .populate("patient doctor")
    .then((result) => {
      return res.status(200).json({
        success: true,
        data: result,
        message: "Medical record fetched successfully.",
      });
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: "Unable to fetch medical records.",
      });
    });
};

exports.changeOwnership = function (req, res) {
  const medicalrecordId = req.body.medicalrecordId;
  const newOwnerEmail = req.body.newOwnerMail;
  const newOwnerId = req.body.newOwnerId;

  const currentUserId = req.body.currentUserId;

  User.findOne({
    email: newOwnerEmail,
  })
    .then((user) => {
      if (user != null) {
        const ownership = {
          $class: "com.mycontext.ChangeMedicalRecordOwner",
          medicalRecord:
            "resource:com.mycontext.MedicalRecord#" + medicalrecordId,
          newOwner: "resource:com.mycontext.Owner#" + user._id,
        };

        Request.post(
          {
            headers: {
              "content-type": "application/json",
            },
            url:
              config.get("blockchainUrl") +
              "api/com.mycontext.ChangeMedicalRecordOwner",
            body: JSON.stringify(ownership),
          },
          (error, response, body) => {
            if (error) {
              return res.status(500).json({
                success: false,
                message: "Failed to change the ownership.",
              });
            }
            console.log("before update", newOwnerId);

            BuyRecords.updateOne(
              {
                userId: currentUserId,
                recordId: medicalrecordId,
              },
              {
                $set: {
                  userId: newOwnerId,
                },
              }
            )
              .then((result) => {
                console.log("after update", result);
                return res.status(200).json({
                  success: true,
                  message: "Ownership changed successfully..",
                });
              })
              .catch((error) => {
                logger.error("error");

                return res.status(500).json({
                  success: false,
                  message: "Failed to update Ownership.",
                });
              });
          }
        );
      } else {
        return res.status(500).json({
          success: false,
          message: "Unable to find user email.",
        });
      }
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: "Unable to find user email.",
      });
    });
};

exports.deleteRecord = function (req, res) {
  const userId = req.body.userId;
  const id = req.body.id;

  Request.delete(
    {
      url:
        config.get("blockchainUrl") + "api/com.mycontext.MedicalRecord/" + id,
    },
    (error, response, body) => {
      if (error) {
        return res.status(500).json({
          success: false,
          message: "Failed to delete medical record.",
        });
      }

      Record.findByIdAndDelete(id)
        .then((result) => {
          return res.status(200).json({
            success: true,
            message: "Medical record deleted successfully.",
          });
        })
        .catch((error) => {
          logger.error("error");

          return res.status(500).json({
            success: false,
            message: "Failed to delete medical record.",
          });
        });
    }
  );
};

exports.getCountforCancerType = function (req, res) {
  const userId = req.body.userId;
  const cancerType = req.body.cancerType;
  if (!userId || !cancerType) {
    return res.status(500).json({
      success: false,
      message: "Please send a user id and a cancer type",
    });
  } else {
    User.findById(userId)
      .then((data) => {
        if (data) {
          Record.countDocuments({
            cancer_type: cancerType,
          })
            .then((count) => {
              if (count != null) {
                return res.status(200).json({
                  success: true,
                  message: "Count for the records has been derived",
                  count: count,
                });
              } else {
                return res.status(200).json({
                  success: true,
                  message: "Count for the records has been derived",
                  count: 0,
                });
              }
            })
            .catch((error) => {
              logger.error("error");

              return res.status(500).json({
                success: true,
                message: error.message,
              });
            });
        } else {
          return res.status(500).json({
            success: false,
            message: "User not found",
          });
        }
      })
      .catch((error) => {
        logger.error("error");

        return res.status(500).json({
          success: false,
          message: error.message,
        });
      });
  }
};

exports.getAllCancerTypes = function (req, res) {
  const userId = req.body.userId;
  if (!userId) {
    return res.status(500).json({
      success: false,
      message: "User is not authorised",
    });
  } else {
    Record.find()
      .distinct("cancer_type")
      .then((cancerTypes) => {
        var data = {
          cancerTypes: cancerTypes,
        };
        return res.status(200).json({
          success: true,
          message: "Retrieved the cancer types successfully",
          data: data,
        });
      })
      .catch((error) => {
        logger.error("error");

        return res.status(500).json({
          success: false,
          message: "Error while retrieving the cancer types ".concat(
            error.message
          ),
        });
      });
  }
};

exports.getBuyerswithPermission = function (req, res) {
  const userId = req.body.userId;
  const teamId = req.body.teamId;
  if (teamId) {
    //Logic for Patient
    getBuyerswithPermissionInfo(teamId, res);
  } else {
    //Logic for Doctor
    var query = {};
    query["members.user"] = userId;
    Team.find(query)
      .then((teams) => {
        if (teams && teams.length) {
          async.each(
            teams,
            function (team, callback) {
              getBuyerswithPermissionInfo(team._id, res);
            },
            function (err) {
              cb();
            }
          );
        } else {
          cb("User isn't part of any teams!");
        }
      })
      .catch((err) => {
        return res.status(500).json({
          success: false,
          message: err.message,
        });
      });
  }
};

exports.uploadFileToRecord = function (req, res) {
  var doctorId = req.body.userId;
  var recordId;
  var patientId;
  var teamId;
  var doctorInfo;
  var recordInfo;
  var fileName;
  var mimeType;
  var fileId;
  var fileType;
  var busboy = new Busboy({
    headers: req.headers,
  });
  busboy.on("file", function (fieldname, file, filename, encoding, mimetype) {
    var gfs = Grid(mongoose.connection.db, mongoose.mongo);
    fileName = filename;
    mimeType = mimetype;
    var streamwrite = gfs.createWriteStream({
      filename: filename,
      mode: "w",
      metadata: mimetype,
    });
    file.pipe(streamwrite);
    fileId = streamwrite.id;
  });
  busboy.on("field", function (
    fieldname,
    val,
    fieldnameTruncated,
    valTruncated,
    encoding,
    mimetype
  ) {
    if (fieldname == "recordId") recordId = val;
    if (fieldname == "teamId") teamId = val;
    if (fieldname == "patientId") patientId = val;
    if (fieldname == "fileType") fileType = val;
  });
  busboy.on("finish", function () {
    if (recordId && teamId && doctorId && patientId && fileId) {
      async.series(
        [
          function (cb) {
            User.findById(doctorId, function (err, data) {
              if (err) {
                cb(err);
              } else {
                if (data) {
                  doctorInfo = data;
                  cb();
                } else {
                  cb("Doctor not found!");
                }
              }
            });
          },
          function (cb) {
            Record.findOne(
              {
                _id: recordId,
                patient: patientId,
                doctor: doctorId,
                team: teamId,
              },
              function (err, data) {
                if (err) cb(err);
                else {
                  if (data) {
                    recordInfo = data;
                    cb();
                  } else {
                    cb("No valid record exists to add the file!");
                  }
                }
              }
            );
          },
        ],
        function (err, data) {
          if (err) {
            if (fileId) removeFile(fileId);
            return res.status(500).json({
              success: false,
              message: err,
            });
          } else {
            var file = {
              fileName: fileName,
              mimeType: mimeType,
              uploaded_by: doctorInfo["email"],
              fileType: fileType,
              uploaded_on: Date.now(),
              grid_id: fileId.toString(),
            };
            if (recordInfo["files"]) {
              recordInfo["files"].push(file);
            } else {
              recordInfo["files"] = [];
              recordInfo["files"].push(file);
            }
            Record.findByIdAndUpdate(recordId, recordInfo, function (
              error,
              data
            ) {
              if (error) {
                return res.status(500).json({
                  success: false,
                  message: error.message,
                });
              } else {
                sendUploadConfirmationMail(patientId, file);
                return res.status(200).json({
                  success: true,
                  message: "Upload Successful",
                });
              }
            });
          }
        }
      );
    } else {
      if (fileId) removeFile(fileId);
      return res.status(500).json({
        success: false,
        message: "Check file and all other inputs",
      });
    }
  });
  return req.pipe(busboy);
};

function removeFile(fileId) {
  var gfs = Grid(mongoose.connection.db, mongoose.mongo);
  gfs.remove(
    {
      _id: fileId,
    },
    function (err) {
      if (err) logger.error("couldn't delete file with id ", fileId);
      else logger.error("Deleted file with id ", fileId);
    }
  );
}

function sendUploadConfirmationMail(patientId, file) {
  User.findById(patientId, function (err, data) {
    if (err) logger.error("Error while sending mail to " + patientId);
    else {
      if (data) {
        var email = config.get("email");
        var password = config.get("password");
        var transporter = nodemailer.createTransport({
          service: "gmail",
          auth: {
            user: email,
            pass: password,
          },
        });
        var mailOptions = {
          from: email,
          to: data["email"],
          subject: "MyContext: File uploaded to your medical record!",
          html:
            "<p>Hello " +
            data["name"] +
            ",</p><p> Doctor " +
            file["uploaded_by"] +
            " has uploaded file " +
            file["fileName"] +
            ".</p><p> Please " +
            'click <a href="http://mycontext-frontend.herokuapp.com/my-team">here</a> to check.</p><p>Regards,</p><p>MyContext Team</p>',
        };
        transporter
          .sendMail(mailOptions)
          .then((result) => {
            logger.info("Sent file upload mail to " + data["email"]);
          })
          .catch((error) => {
            logger.error(
              "Error while sending fiel upload mail to " + data["email"]
            );
          });
      } else {
        logger.error("Didnt find patient with id " + patientId);
      }
    }
  });
}
exports.downloadFileFromRecord = function (req, res) {
  var userId = req.body.userId;
  var teamId = req.body.teamId;
  var recordId = req.body.recordId;
  var fileId = req.body.fileId;
  var isPatient = false;
  var isDoctor = false;
  var fileInfo;
  async.series(
    [
      function (cb) {
        User.findById(userId, function (err, data) {
          if (data) {
            if (data["user_type"] == constant.CONSTANTS.userTypes.PATIENT) {
              isPatient = true;
            } else if (
              data["user_type"] == constant.CONSTANTS.userTypes.DOCTOR
            ) {
              isDoctor = true;
            }
            cb();
          } else {
            cb("User not found!");
          }
        });
      },
      function (cb) {
        var query = {
          team: teamId,
          _id: recordId,
        };
        if (isPatient) query["patient"] = userId;
        if (isDoctor) query["doctor"] = userId;
        Record.findOne(query, function (err, data) {
          if (data) {
            var files = data["files"];
            if (files && files.length) {
              var filteredFiles = files.filter(
                (file) => file.grid_id == fileId
              );
              if (filteredFiles && filteredFiles.length) {
                fileInfo = filteredFiles[0];
              }
            }
            if (fileInfo) cb();
            else cb("File not part of the record!");
          } else {
            cb("Record not found!");
          }
        });
      },
    ],
    function (err, data) {
      if (err) {
        return res.status(500).json({
          success: false,
          message: err,
        });
      } else {
        var gfs = Grid(mongoose.connection.db, mongoose.mongo);
        var readstream = gfs.createReadStream({
          _id: fileId,
        });
        res.attachment(fileInfo["fileName"]);
        res.setHeader(
          "Content-disposition",
          "attachment; filename=".concat(fileInfo["fileName"])
        );
        res.setHeader("Content-type", fileInfo["mimeType"]);
        readstream.pipe(res);
        readstream.on("close", function (file) {
          logger.info("File {} Read successfully from database", fileId);
        });
      }
    }
  );
};

function getBuyerswithPermissionInfo(teamId, res) {
  Record.find(
    {
      team: teamId, //considering every patient will have one one record
    },
    function (err, recordInfo) {
      if (err) cb();
      else {
        var recordId = recordInfo[0]["_id"];
        Bid.find({
          recordBought: {
            $in: [recordId],
          },
        })
          .populate("buyer")
          .exec(function (err, bidInfo) {
            if (err) {
              console.log(err);
              cb("bids not found");
            } else {
              if (bidInfo && bidInfo.length) {
                var entry = bidInfo.map((bid) => {
                  return {
                    buyerId: bid.buyer._id,
                    bidBuyer: bid.buyer,
                  };
                });
                var buyers = Array.from(new Set(entry.map(JSON.stringify))).map(
                  JSON.parse
                );
                return res.status(200).json({
                  success: true,
                  message: "Retrieved the buyers successfully",
                  data: buyers,
                });
              }
            }
          });
      }
    }
  );
}
