const User = require("../models/user.model");
const Team = require("../models/team.model");
const BuyRecords = require("../models/buy.model");
const Token = require("../models/token.model");
const Record = require("../models/record.model");
const Bid = require("../models/bid.model");
const crypto = require("crypto");
const nodemailer = require("nodemailer");
const jwt = require("jsonwebtoken");
const Request = require("request");
const config = require("config");
const mongoose = require("mongoose");
const validator = require("email-validator");
const logger = require("../utils/logger");
const constant = require("../utils/constants");

exports.listUsers = function (req, res) {
  User.find({
    user_type: "Patient",
  })
    .select("_id", "name", "email")
    .then((users) => {
      return res.status(500).json({
        success: true,
        message: "Successfully fetched users list.",
        data: users,
      });
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};

exports.buyRecord = function (req, res) {
  const data = req.body;
  const newRecord = new BuyRecords();
  newRecord.cardName = data.name;
  newRecord.cardNumber = data.cardNumber;
  newRecord.expiryMonth = data.month;
  newRecord.expiryYear = data.year;
  newRecord.cvv = data.cvv;
  newRecord.recordId = data.recordId;
  newRecord.userId = data.userId;
  newRecord
    .save()
    .then((result) => {
      return res.json({
        success: true,
        message: "Payment done successfully.",
      });
    })
    .catch((error) => {
      logger.error("error");

      res.json({
        success: false,
        message: "Payment failed",
      });
    });
};

exports.register = function (req, res) {
  User.findOne({
    email: req.body.email,
  })
    .then((user) => {
      if (user) {
        return res.status(500).json({
          success: false,
          message: "User already exists.",
        });
      } else {
        // if (req.body.user_type === constant.CONSTANTS.userTypes.DOCTOR) {
        //   if (req.body.registrationNumber == undefined) {
        //     return res.status(500).json({
        //       success: false,
        //       message: "Regsitration number is mandatory for doctor to sign up",
        //     });
        //   } else {
        //   }
        // } else if (
        //   req.body.user_type === constant.CONSTANTS.userTypes.ORGANZATION
        // ) {
        //   if (req.body.abn == undefined) {
        //     return res.status(500).json({
        //       success: false,
        //       message: "Regsitration number is mandatory for doctor to sign up",
        //     });
        //   }
        // }
        const new_user = new User({
          name: req.body.name,
          email: req.body.email,
          password: req.body.password,
          user_type: req.body.user_type,
        });

        new_user
          .save()
          .then((result) => {
            Request.post(
              {
                headers: {
                  "content-type": "application/json",
                },
                url: config.get("blockchainUrl") + "api/com.mycontext.Owner",
                body: JSON.stringify({
                  $class: "com.mycontext.Owner",
                  ownerId: new_user._id,
                  name: new_user.name,
                }),
              },
              function (err, httpResponse, body) {
                if (err) {
                  result.remove();
                  return res.json({
                    success: false,
                    message: "User registration failed.",
                  });
                }
                /** Creating team if user is a patient*/
                if (req.body.user_type === "Patient") {
                  const team = new Team({
                    name: req.body.email,
                    admin: result._id,
                  });
                  team
                    .save()
                    .then((success) => {
                      return sendVerificationMail(result, req, res);
                    })
                    .catch((error) => {
                      logger.error("error");

                      return res.status(500).json({
                        success: false,
                        message: error.message,
                      });
                    });
                } else {
                  return sendVerificationMail(result, req, res);
                }

                // return res.status(200).json({
                //   success: true,
                //   message: "User registration successfull"
                // });
              }
            );
          })
          .catch((error) => {
            logger.error("error");

            return res.status(500).json({
              success: false,
              message: error.message,
            });
          });
      }
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};

exports.login = function (req, res) {
  User.findOne({
    email: req.body.email,
  })
    .then((user) => {
      if (user) {
        user.comparePassword(req.body.password, function (err, isMatch) {
          if (err) {
            return res.status(500).json({
              success: false,
              message: err.message,
            });
          }

          if (isMatch) {
            if (user.isVerified) {
              if (user.user_type != "Patient") {
                const payload = {
                  userId: user._id,
                };

                var token = jwt.sign(payload, config.get("secret"));

                res.status(200).json({
                  success: true,
                  message: "Access token generation successfull.",
                  token: token,
                  name: user.name,
                  email: user.email,
                  user_id: user._id,
                  user_type: user.user_type,
                });
              } else {
                Team.findOne({
                  admin: user._id,
                })
                  .then((team) => {
                    if (team) {
                      const payload = {
                        userId: user._id,
                        teamId: team._id,
                      };

                      var token = jwt.sign(payload, config.get("secret"));

                      res.status(200).json({
                        success: true,
                        message: "Access token generation successfull.",
                        token: token,
                        name: user.name,
                        email: user.email,
                        user_id: user._id,
                        user_type: user.user_type,
                        teamName: team.name,
                      });
                    } else {
                      res.status(500).json({
                        success: false,
                        message: "No team associated with user",
                      });
                    }
                  })
                  .catch((error) => {
                    logger.error("error");

                    res.status(500).json({
                      success: false,
                      message: error.message,
                    });
                  });
              }
            } else {
              res.status(500).json({
                success: false,
                message: "Please verify your account",
                token: token,
                name: user.name,
                email: user.email,
                user_type: user.user_type,
              });
            }
          } else {
            return res.status(500).json({
              success: false,
              message: "Authentication failed. Wrong password.",
            });
          }
        });
      } else {
        return res.json({
          success: false,
          message: "Authentication failed. User not found.",
        });
      }
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};

exports.confirmation = function (req, res) {
  Token.findOne({
    token: req.params.token,
  }).then((token) => {
    if (!token) {
      return res.status(200).json({
        success: false,
        message:
          "We were unable to find a valid token. Your token my have expired.",
      });
    }

    User.findOne({
      _id: token._userId,
    })
      .then((user) => {
        if (!user) {
          return res.status(200).json({
            success: false,
            message: "We were unable to find a user for this token.",
          });
        }

        if (user.isVerified) {
          return res.status(200).json({
            success: false,
            message: "This user has already been verified.",
          });
        }

        user.isVerified = true;

        user
          .save()
          .then((result) => {
            sendWelcomeMail(res, user.name, user.email);
          })
          .catch((error) => {
            logger.error("error");

            return res.status(500).json({
              success: false,
              message: error.message,
            });
          });
      })
      .catch((error) => {
        logger.error("error");

        return res.status(500).json({
          success: false,
          message: error.message,
        });
      });
  });
};

sendVerificationMail = function (user, req, res) {
  console.log("send mail");
  var token = new Token({
    _userId: user._id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  console.log("token", token);

  token
    .save()
    .then((result) => {
      var email = config.get("email");
      var password = config.get("password");
      var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: email,
          pass: password,
        },
      });

      var link = config.get("url") + "/confirm-email/" + token.token;

      var mailOptions = {
        from: email,
        to: user.email,
        subject: "MyContext account verification",
        text: "Click to activate your account " + link,
      };

      transporter
        .sendMail(mailOptions)
        .then((result) => {
          res.status(200).json({
            success: true,
            message:
              "A verification email has been sent to " + user.email + ".",
          });
        })
        .catch((error) => {
          logger.error("error");

          return res.status(500).json({
            success: false,
            message: error.message,
          });
        });
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};

exports.forgotPassword = function (req, res) {
  const email = req.body.email;

  User.findOne({
    email: email,
  })
    .then((result) => {
      if (result) {
        const token = crypto.randomBytes(16).toString("hex");
        const expire = Date.now() + 86400000;

        result
          .updateResetPassword(token, expire)
          .then((user) => {
            var email = config.get("email");
            var password = config.get("password");
            var transporter = nodemailer.createTransport({
              service: "gmail",
              auth: {
                user: email,
                pass: password,
              },
            });

            var link = config.get("url") + "/reset-password/" + token;

            var mailOptions = {
              from: email,
              to: user.email,
              subject: "MyContext Password Reset",
              text: "Click to reset your password " + link,
            };

            transporter
              .sendMail(mailOptions)
              .then((result) => {
                res.status(200).json({
                  success: true,
                  message: "A reset link has been sent to " + user.email + ".",
                });
              })
              .catch((error) => {
                logger.error("error");

                return res.status(500).json({
                  success: false,
                  message: error.message,
                });
              });
          })
          .catch((error) => {
            logger.error("error");

            return res.status(500).json({
              success: false,
              message: error.message,
            });
          });
      } else {
        return res.status(500).json({
          success: false,
          message: "Email not found",
        });
      }
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};

exports.changePassword = function (req, res) {
  const token = req.body.token;
  const password = req.body.password;

  User.findOne({
    resetPasswordToken: token,
    resetPasswordExpires: {
      $gt: Date.now(),
    },
  })
    .then((result) => {
      if (result) {
        result
          .changePassword(password)
          .then((user) => {
            return res.status(200).json({
              success: true,
              message: "Password changed successfully",
            });
          })
          .catch((error) => {
            logger.error("error");

            return res.status(500).json({
              success: false,
              message: error.message,
            });
          });
      } else {
        return res.status(200).json({
          success: false,
          message: "Token has expired",
        });
      }
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};

exports.addTeamMember = function (req, res) {
  const data = req.body;
  var admin = data.userId;
  if (data.doctorIds && data.doctorIds.length) {
    Team.findOne({
      admin: admin,
    })
      .lean()
      .then((result) => {
        if (result) {
          if (result.members && result.members.length) {
            var convArray = result.members.map((members) =>
              members.id.toString()
            );
            var givenDoctorIds = data.membersToBeAdded.map(
              (member) => member.user
            );
            for (var i = 0; i < givenDoctorIds.length; i++) {
              if (!convArray.includes(givenDoctorIds[i])) {
                if (
                  data.membersToBeAdded[i].share != null &&
                  result.adminShare > 0
                ) {
                  result.adminShare -= data.membersToBeAdded[i].share;
                } else {
                  data.membersToBeAdded[i].share = 0;
                }
                data.membersToBeAdded[i].id = mongoose.Types.ObjectId(
                  data.membersToBeAdded[i].id
                );
                result.members.push(data.membersToBeAdded[i]);
              }
            }
          } else {
            var allRecords = data.membersToBeAdded.map(
              (mbr) => (mbr.id = mongoose.Types.ObjectId(mbr.id))
            );
            for (var i = 0; i < allRecords.length; i++) {
              if (allRecords[i].share != null)
                result.adminShare -= allRecords[i].share;
              else allRecords[i].share = 0;
            }
            result.members = [...allRecords];
          }
          var update = {
            adminShare: result.adminShare,
            members: result.members,
          };
          Team.findByIdAndUpdate(result._id, update)
            .then((result) => {
              return res.status(200).json({
                success: true,
                message: "Added the members successfully to the team",
              });
            })
            .catch((error) => {
              logger.error("error");

              return res.status(500).json({
                success: false,
                message: error.message,
              });
            });
        } else {
          return res.status(500).json({
            success: false,
            message: "Team couldn't be found for given patient",
          });
        }
      })
      .catch((error) => {
        logger.error("error");

        return res.status(500).json({
          success: false,
          message: error.message,
        });
      });
  } else {
    return res.status(500).json({
      success: false,
      message: "No members are sent to be added to the team",
    });
  }
};

sendWelcomeMail = function (res, name, userEmail) {
  var email = config.get("email");
  var password = config.get("password");
  var transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: email,
      pass: password,
    },
  });

  var mailOptions = {
    from: email,
    to: userEmail,
    subject: "Welcome to MyContext",
    html: "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head> <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /> <title>MyContext</title> <meta name='viewport' content='width=device-width, initial-scale=1.0' /> <style> button { display: inline-block; border: none; padding: 1rem 2rem; margin: 0; text-decoration: none; background: #5e72e4; color: #ffffff; font-family: sans-serif; font-size: 1rem; cursor: pointer; border-radius: 6px; text-align: center; transition: background 250ms ease-in-out, transform 150ms ease; -webkit-appearance: none; -moz-appearance: none; } button:hover, button:focus { background: #5e72e4; } button:focus { outline: 1px solid #fff; outline-offset: -4px; } button:active { transform: scale(0.99); } </style></head><body style='background: #ecf0f1'> <table align='center' border='0' cellpadding='20' cellspacing='20' width='600' style='border-collapse: collapse;background: #fff;border-radius: 6px'> <tr> <td style='color: #5e72e4; font-family: Arial, sans-serif; font-size: 30px; line-height: 20px;padding-bottom: 24px'> <b>Welcome To MyContext</b> </td> </tr> <tr> <td style='color: #9e9e9e; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;padding-bottom: 24px'> Hi <b style='color: #000000; font-family: Arial, sans-serif; font-size: 16px;'>{{username}}</b>,</td></tr><tr><td style='color: #9e9e9e; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;'> Well done. By signing up, you have taken your first step towords a happier, healthier life. We will do everything we can to help and support you as you sell your data.</td> </tr> <tr> <td align='center' style='color: #5e72e4; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;padding-bottom: 24px'> <button style='background-color:green'><a href='https://mycontext-web.herokuapp.com/login' style='color:white;text-decoration: none'>Login</a></button> </td> </tr> <td bgcolor='#5e72e4' style='padding: 30px 30px 30px 30px;border-bottom-left-radius: 6px;border-bottom-right-radius: 6px'> <table border='0' cellpadding='0' cellspacing='0' width='100%'> <tr> <td width='75%' style='color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;'> Copyright © 2020, MyContext </td> <td align='right'> <table border='0' cellpadding='0' cellspacing='0'> <tr> <td> <a href='http://www.twitter.com/'> <img src='https://www.iconsdb.com/icons/preview/white/twitter-xxl.png' alt='Twitter' width='38' height='38' style='display: block;' border='0' /> </a> </td> <td style='font-size: 0; line-height: 0;' width='20'>&nbsp;</td> <td> <a href='http://www.facebook.com/'> <img src='https://www.iconsdb.com/icons/preview/white/facebook-3-xxl.png' alt='Facebook' width='38' height='38' style='display: block;' border='0' /> </a> </td> </tr> </table> </td> </tr> </table> </td> </table></body></html>".replace(
      "{{username}}",
      name
    ),
  };

  transporter
    .sendMail(mailOptions)
    .then((result) => {
      return res.status(200).json({
        success: true,
        message: "The account has been verified. Please log in",
      });
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};

// Api to search doctor by email or name
exports.searchDoctorbyEmailorName = function (req, res) {
  const mailIdOrName = req.body.mailIdOrName;
  isValidMailId = validator.validate(mailIdOrName); // true

  User.find({
    user_type: {
      $ne: "Patient",
    },
    $or: [
      {
        name: new RegExp(mailIdOrName, "gi"), //case-sensitive search
      },
      {
        email: new RegExp(mailIdOrName, "gi"),
      },
    ],
  })
    .then((result) => {
      console.log(result);
      return res.status(200).json({
        success: true,
        data: result,
        message: "Doctor records fetched successfully.",
      });
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};

// Api to add team member by mail id
exports.addTeamMemberByMailId = function (req, res) {
  const data = req.body;
  var teamId = data.teamId;
  const mailId = data.emailAddress;
  var share = data.share;
  isValidMailId = validator.validate(mailId); // true
  if (!isValidMailId) {
    return res.status(500).json({
      success: false,
      message: "Invalid Email Address",
    });
  } else {
    User.findOne({
      email: mailId,
      user_type: {
        $ne: "Patient",
      },
    })
      .then((teamData) => {
        if (teamData) {
          var userIdOfDoctor = teamData._id;

          Team.findOne({
            _id: teamId,
          })
            .lean()
            .then((result) => {
              if (result) {
                if (share != null && result.adminShare > 0) {
                  result.adminShare -= share;
                } else {
                  share = 0;
                }
                if (result.members && result.members.length) {
                  var convArray = result.members.map((mbr) =>
                    mbr.user.toString()
                  );
                  if (!convArray.includes(teamData.id)) {
                    result.members.push({
                      user: mongoose.Types.ObjectId(userIdOfDoctor),
                      share: share,
                    });
                  } else {
                    return res.status(500).json({
                      success: false,
                      message: "Team member already exists",
                    });
                  }
                } else {
                  result.members = [
                    {
                      user: mongoose.Types.ObjectId(userIdOfDoctor),
                      share: share,
                    },
                  ];
                }
                var update = {
                  members: result.members,
                  adminShare: result.adminShare,
                };
                Team.findByIdAndUpdate(teamId, update)
                  .then((result) => {
                    return res.status(200).json({
                      success: true,
                      message: "Added the member successfully to the team",
                    });
                  })
                  .catch((error) => {
                    logger.error("error");

                    return res.status(500).json({
                      success: false,
                      message: error.message,
                    });
                  });
              } else {
                return res.status(500).json({
                  success: false,
                  message: "Team couldn't be found for given patient",
                });
              }
            })
            .catch((error) => {
              logger.error("error");

              return res.status(500).json({
                success: false,
                message: error.message,
              });
            });
          return true;
        } else {
          return res.status(500).json({
            success: false,
            message: "No matching record found",
          });
        }
      })
      .catch((error) => {
        logger.error("error");

        return res.status(500).json({
          success: false,
          message: error.message,
        });
      });
  }
};

exports.getCounts = function (req, res) {
  const patientsCount = new Promise((resolve, reject) => {
    User.countDocuments({ user_type: constant.CONSTANTS.userTypes.PATIENT })
      .then((count) => {
        resolve(count);
      })
      .catch((error) => {
        reject(error);
      });
  });

  const doctorsCount = new Promise((resolve, reject) => {
    User.countDocuments({ user_type: constant.CONSTANTS.userTypes.DOCTOR })
      .then((count) => {
        resolve(count);
      })
      .catch((error) => {
        reject(error);
      });
  });

  const medicalRecordsCount = new Promise((resolve, reject) => {
    Record.countDocuments()
      .then((count) => {
        resolve(count);
      })
      .catch((error) => {
        reject(error);
      });
  });

  const bidsCount = new Promise((resolve, reject) => {
    Bid.countDocuments()
      .then((count) => {
        resolve(count);
      })
      .catch((error) => {
        reject(error);
      });
  });

  Promise.all([patientsCount, doctorsCount, medicalRecordsCount, bidsCount])
    .then(([patients, doctor, medicalRecords, bids]) => {
      res.status(200).json({
        success: true,
        patientsCount: patients,
        doctorCount: doctor,
        medicalRecordsCount: medicalRecords,
        bidsCount: bids,
        message: "Counts fetched successfully",
      });
    })
    .catch((error) => {
      res.status(500).json({ success: false, message: error.message });
    });
};

exports.getGraphsData = function (req, res) {
  const cancerByType = new Promise((resolve, reject) => {
    Record.aggregate([{ $group: { _id: "$cancer_type", count: { $sum: 1 } } }])
      .then((cancerByType) => {
        resolve(cancerByType);
      })
      .catch((error) => {
        reject(error);
      });
  });

  const ageGroup = new Promise((resolve, reject) => {
    Record.aggregate([
      {
        $group: {
          _id: {
            $cond: [
              { $gt: ["$year_of_birth", 2002] },
              "Below 18",
              {
                $cond: [
                  {
                    $and: [
                      { $gt: ["$year_of_birth", 1990] },
                      { $lt: ["$year_of_birth", 2002] },
                    ],
                  },
                  "19-30",
                  {
                    $cond: [
                      {
                        $and: [
                          { $gt: ["$year_of_birth", 1970] },
                          { $lt: ["$year_of_birth", 1990] },
                        ],
                      },
                      "31-50",
                      "Above 50",
                    ],
                  },
                ],
              },
            ],
          },
          count: { $sum: 1 },
        },
      },
    ])
      .then((ageGroup) => {
        resolve(ageGroup);
      })
      .catch((error) => {
        reject(error);
      });
  });

  const byGender = new Promise((resolve, reject) => {
    Record.aggregate([{ $group: { _id: "$gender", count: { $sum: 1 } } }])
      .then((byGender) => {
        resolve(byGender);
      })
      .catch((error) => {
        reject(error);
      });
  });

  const byBloodGroup = new Promise((resolve, reject) => {
    Record.aggregate([{ $group: { _id: "$blood_group", count: { $sum: 1 } } }])
      .then((byBloodGroup) => {
        resolve(byBloodGroup);
      })
      .catch((error) => {
        reject(error);
      });
  });

  Promise.all([cancerByType, ageGroup, byGender, byBloodGroup])
    .then(([cancerByType, ageGroup, byGender, byBloodGroup]) => {
      res.status(200).json({
        success: true,
        cancerByType: cancerByType,
        ageGroup: ageGroup,
        byGender: byGender,
        byBloodGroup: byBloodGroup,
        message: "Counts fetched successfully",
      });
    })
    .catch((error) => {
      res.status(500).json({ success: false, message: error.message });
    });
};

exports.getUserDetails = function (req, res) {
  const userId = req.body.userId;
  if (userId) {
    User.findById(userId, function (err, result) {
      if (err) {
        res.status(500).json({ success: false, message: err.message });
      } else {
        if (result) {
          res.status(200).json({
            success: true,
            message: "Details retrieved",
            data: result,
          });
        } else {
          res.status(500).json({ success: false, message: "User not found" });
        }
      }
    });
  } else {
    res.status(500).json({ success: false, message: "User Id not passed" });
  }
};
