const User = require("../models/user.model");
const Bid = require("../models/bid.model");
const Team = require("../models/team.model");
const BidTransaction = require("../models/bidTransaction.model");
const constant = require("../utils/constants");
const Record = require("../models/record.model");
const async = require("async");
const logger = require("../utils/logger");
const config = require("config");

stripeApiKey = config.get("stripeSecretKey")
var stripe = require('stripe')(stripeApiKey);

exports.createBid = function (req, res) {
    const userId = req.body.userId;
    User.findById(userId)
        .then((result) => {
            if (result) {
                const bid = new Bid({
                    bidTitle: result.name + "_" + req.body.cancerType + "_",
                    cancerType: req.body.cancerType,
                    recordsRequired: req.body.recordsRequired,
                    recordsAcquired: 0,
                    bidStartDate: req.body.bidStartDate,
                    bidExpiryDate: req.body.bidExpiryDate,
                    amountPerRecord: req.body.amountPerRecord,
                    bidStatus: req.body.bidStatus,
                    recordAccessStartDate: req.body.recordAccessStartDate,
                    recordAccessExpiryDate: req.body.recordAccessExpiryDate,
                    buyer: userId,
                    teamId: req.body.teamId,
                });
                bid.save().then((bidData) => {
                    return res.status(200).json({
                        success: true,
                        message: "Bid Successfully Created",
                        data: bidData,
                    });
                });
            } else {
                return res.status(500).json({
                    success: false,
                    message: "User not found",
                });
            }
        })
        .catch((error) => {
            logger.error("error");

            return res.status(500).json({
                success: false,
                message: error.message,
            });
        });
};
exports.getBuyersBids = function (req, res) {
    const from = req.body.from || 0;
    const size = req.body.size || 10;
    const userId = req.body.userId;
    Bid.find({
            buyer: userId,
            bidExpiryDate: {
                $gte: new Date(),
            },
        })
        .skip(from)
        .limit(size)
        .then((result) => {
            return res.status(200).json({
                success: true,
                data: result,
                message: "Bids fetched successfully.",
            });
        })
        .catch((error) => {
            logger.error("error");

            return res.status(500).json({
                success: false,
                message: error.message,
            });
        });
};

exports.getSellerBids = function (req, res) {
    const userId = req.body.userId;
    const status = req.body.status || 1;

    switch (status) {
        default:
        case 1:
            getIncomingBids(userId, res);
            break;
        case 2:
            getActionedBids(userId, 2, res);
            break;
        case 3:
            getActionedBids(userId, 3, res);
            break;
    }
};

function getIncomingBids(userId, res) {
    var user;
    var teams;
    var actionedBids = [];
    var isPatient = false;
    var response = [];
    async.series(
        [
            function (cb) {
                User.findById(userId, function (err, result) {
                    if (err) cb(err);
                    else {
                        user = result;
                        if (user) cb();
                        else cb("User not found!");
                    }
                });
            },
            function (cb) {
                var query = {};
                if (user && user.user_type === constant.CONSTANTS.userTypes.PATIENT) {
                    isPatient = true;
                    query.admin = userId;
                } else {
                    query["members.user"] = userId;
                }
                Team.find(query, function (err, result) {
                    if (err) cb(err);
                    else {
                        if (result && result.length) {
                            teams = result;
                            cb();
                        } else {
                            cb("User isn't part of any teams!");
                        }
                    }
                });
            },
            function (cb) {
                BidTransaction.find({
                        seller: userId,
                    })
                    .distinct("bid")
                    .exec(function (err, result) {
                        if (err) cb(err);
                        else {
                            if (result && result.length)
                                actionedBids = actionedBids.concat(result);
                            cb();
                        }
                    });
            },
            function (cb) {
                async.each(
                    teams,
                    function (team, callback) {
                        var share = getShare(team, userId, isPatient);
                        Record.find({
                                team: team._id,
                            },
                            function (err, result) {
                                if (err) callback(err);
                                else {
                                    if (result && result.length) {
                                        cancerType = result[0]["cancer_type"];
                                        var query = {
                                            cancerType: cancerType,
                                            bidExpiryDate: {
                                                $gte: new Date(),
                                            },
                                            bidStartDate: {
                                                $lte: new Date(),
                                            },
                                            bidStatus: constant.CONSTANTS.bidStatus.ACTIVE,
                                        };
                                        if (actionedBids && actionedBids.length) {
                                            query._id = {
                                                $nin: actionedBids,
                                            };
                                        }
                                        Bid.find(query)
                                            .populate("buyer")
                                            .exec(function (err, bids) {
                                                if (err) callback(err);
                                                else {
                                                    if (bids && bids.length) {
                                                        var entry = bids.map((bid) => {
                                                            return {
                                                                teamName: team.name,
                                                                bidName: bid.bidTitle,
                                                                bidExpiry: bid.bidExpiryDate,
                                                                bidAmountPerRecord: bid.amountPerRecord,
                                                                bidPersonalEarning: bid.amountPerRecord * (share / 100),
                                                                recordId: result[0]["_id"],
                                                                teamId: team._id,
                                                                bidId: bid._id,
                                                                bidBuyer: bid.buyer,
                                                            };
                                                        });
                                                        response = response.concat(entry);
                                                        callback();
                                                    } else {
                                                        callback();
                                                    }
                                                }
                                            });
                                    } else {
                                        callback("No records found for placing bid");
                                    }
                                }
                            }
                        );
                    },
                    function (error, data) {
                        if (error) cb(error);
                        else cb();
                    }
                );
            },
        ],
        function (error, data) {
            if (error) {
                return res.status(500).json({
                    success: false,
                    message: error,
                });
            } else {
                return res.status(200).json({
                    success: true,
                    message: "Retrieved the bids successfully",
                    data: response,
                });
            }
        }
    );
}

function getActionedBids(userId, status, res) {
    var user;
    var teams;
    var transactions = [];
    var isPatient = false;
    var response = [];
    async.series(
        [
            function (cb) {
                User.findById(userId, function (err, result) {
                    if (err) cb(err);
                    else {
                        user = result;
                        if (user) cb();
                        else cb("User not found!");
                    }
                });
            },
            function (cb) {
                var query = {};
                if (user && user.user_type === constant.CONSTANTS.userTypes.PATIENT) {
                    isPatient = true;
                    query.admin = userId;
                } else {
                    query["members.user"] = userId;
                }
                Team.find(query, function (err, result) {
                    if (err) cb(err);
                    else {
                        if (result && result.length) {
                            teams = result;
                            cb();
                        } else {
                            cb("User isn't part of any teams!");
                        }
                    }
                });
            },
            function (cb) {
                async.each(
                    teams,
                    function (team, callback) {
                        BidTransaction.find({
                                seller: userId,
                                team: team._id,
                                status: "A",
                            })
                            .populate("bid buyer team")
                            .exec(function (err, result) {
                                if (err) callback(err);
                                else {
                                    if (result && result.length) {
                                        result = result.filter(
                                            (trans) => trans.bid.bidExpiryDate >= new Date()
                                        );
                                        transactions = transactions.concat(result);
                                        callback();
                                    } else {
                                        callback();
                                    }
                                }
                            });
                    },
                    function (err) {
                        cb();
                    }
                );
            },
            function (cb) {
                var share;
                if (transactions && transactions.length) {
                    async.each(
                        transactions,
                        function (transaction, callback) {
                            share = getShare(transaction.team, userId, isPatient);
                            checkTeamAcceptance(
                                transaction.team,
                                transaction.bid._id,
                                function (error, result) {
                                    if (error) callback();
                                    else {
                                        if (status == 2 && !result) {
                                            var entry = {
                                                teamName: transaction.team.name,
                                                bidName: transaction.bid.bidTitle,
                                                bidExpiry: transaction.bid.bidExpiryDate,
                                                bidAmountPerRecord: transaction.bid.amountPerRecord,
                                                bidPersonalEarning: transaction.bid.amountPerRecord * (share / 100),
                                                recordId: transaction.recordId,
                                                teamId: transaction.team._id,
                                                bidId: transaction.bid._id,
                                                bidBuyer: transaction.buyer,
                                            };
                                            response.push(entry);
                                            callback();
                                        } else if (status == 3 && result) {
                                            var entry = {
                                                teamName: transaction.team.name,
                                                bidName: transaction.bid.bidTitle,
                                                bidExpiry: transaction.bid.bidExpiryDate,
                                                bidAmountPerRecord: transaction.bid.amountPerRecord,
                                                bidPersonalEarning: transaction.bid.amountPerRecord * (share / 100),
                                                recordId: transaction.recordId,
                                                teamId: transaction.team._id,
                                                bidId: transaction.bid._id,
                                                bidBuyer: transaction.buyer,
                                            };

                                            response.push(entry);
                                            callback();
                                        } else {
                                            callback();
                                        }
                                    }
                                }
                            );
                        },
                        function (err) {
                            cb();
                        }
                    );
                } else {
                    return res.status(200).json({
                        success: true,
                        message: "Retrieved the bids successfully",
                        data: response,
                    });
                }
            },
        ],
        function (err, data) {
            if (err) {
                return res.status(500).json({
                    success: false,
                    message: err,
                });
            } else {
                return res.status(200).json({
                    success: true,
                    message: "Retrieved the bids successfully",
                    data: response,
                });
            }
        }
    );
}

function checkTeamAcceptance(team, bidId, cb) {
    var members = team.members;
    members.push({
        user: team.admin,
        share: team.adminShare,
    });
    async.each(
        members,
        function (member, callback) {
            BidTransaction.find({
                    bid: bidId,
                    team: team._id,
                    seller: member.user,
                })
                .then((result) => {
                    if (result && result.length) {
                        if (result[0].status === "A") {
                            callback();
                        } else callback("Rejected");
                    } else {
                        callback("Not Actioned");
                    }
                })
                .catch((error) => {
                    logger.error("error");

                    callback(error);
                });
        },
        function (error, result) {
            if (error) cb(null, false);
            else cb(null, true);
        }
    );
}

function getShare(team, userId, isPatient) {
    if (isPatient) {
        return team.adminShare;
    } else {
        var members = team.members;
        if (members && members.length) {
            var filtered = members.filter((mbr) => mbr.user.toString() === userId);
            return filtered[0]["share"];
        }
        return 0;
    }
}

exports.acceptBid = function (req, res) {
    const seller = req.body.userId;
    const teamId = req.body.teamId;
    const recordId = req.body.recordId;
    const buyer = req.body.buyerId;
    const bid = req.body.bidId;
    if (!seller) {
        return res.status(500).json({
            success: false,
            message: "Invalid Access token!",
        });
    } else if (!(teamId || recordId || buyer || bid)) {
        return res.status(500).json({
            success: false,
            message: "Please send all required parameters",
        });
    } else {
        var query = {
            seller: seller,
            team: teamId,
            record: recordId,
            buyer: buyer,
            bid: bid,
        };
        BidTransaction.findOneAndUpdate(query, {
            status: "A",
        }).then((result) => {
            if (result) {
                updateRecordsBought(bid, teamId, recordId, res);
            } else {
                var dataToInsert = {
                    ...query,
                };
                dataToInsert.status = "A";
                const newbid = new BidTransaction(dataToInsert)
                    .save()
                    .then((data) => {
                        updateRecordsBought(bid, teamId, recordId, res);
                    })
                    .catch((err) => {
                        return res.status(500).json({
                            success: true,
                            message: "Error while saving bid transaction",
                        });
                    });
            }
        });
    }
};

function updateRecordsBought(bid, teamId, recordId, res) {
    Team.findById(teamId).then((result) => {
        if (result) {
            checkTeamAcceptance(result, bid, function (err, data) {
                if (err) {
                    return res.status(500).json({
                        success: false,
                        message: err,
                    });
                } else {
                    if (data) {
                        Bid.findById(bid).then((result) => {
                            if (result) {
                                result.recordAccepted.push(recordId);
                                Bid.findOneAndUpdate({
                                    _id: bid,
                                }, {
                                    recordAccepted: result.recordAccepted,
                                }).then((result) => {
                                    return res.status(200).json({
                                        success: true,
                                        message: "Successfully accepted the bid",
                                    });
                                });
                            } else {
                                return res.status(200).json({
                                    success: true,
                                    message: "Successfully accepted the bid",
                                });
                            }
                        });
                    } else {
                        return res.status(200).json({
                            success: true,
                            message: "Successfully accepted the bid",
                        });
                    }
                }
            });
        } else {
            return res.status(500).json({
                success: false,
                message: "Team not found",
            });
        }
    });
}

exports.getBuyersMaturedBids = function (req, res) {
    const userId = req.body.userId;
    var response = [];
    Bid.find({
            buyer: userId,
            bidExpiryDate: {
                $lt: Date.now(),
            },
            bidStatus: constant.CONSTANTS.bidStatus.ACTIVE,
        })
        .populate("recordAccepted")
        .then((result) => {
            if (result && result.length) {
                async.each(
                    result,
                    function (bid, callback) {
                        recordsAccepted = bid.recordAccepted;
                        if (recordsAccepted && recordsAccepted.length) {
                            async.each(
                                recordsAccepted,
                                function (record, cb) {
                                    var team;
                                    var patient;
                                    async.parallel(
                                        [
                                            function (cbk) {
                                                Team.findById(record.team, function (err, teamData) {
                                                    if (err) cbk(err);
                                                    else {
                                                        team = teamData;
                                                        cbk();
                                                    }
                                                });
                                            },
                                            function (cbk) {
                                                User.findById(record.patient, function (
                                                    err,
                                                    patientData
                                                ) {
                                                    if (err) cbk(err);
                                                    else {
                                                        patient = patientData;
                                                        cbk();
                                                    }
                                                });
                                            },
                                        ],
                                        function (err, data) {
                                            if (err) cb(err);
                                            else {
                                                var entry = {
                                                    bidName: bid.bidTitle,
                                                    bidId: bid._id,
                                                    numberOfRecordsAcquired: bid.recordsAcquired,
                                                    numberOfRecordsRequired: bid.recordsRequired,
                                                    amountPerRecord: bid.amountPerRecord,
                                                    recordAccessStartDate: bid.recordAccessStartDate,
                                                    recordAccessExpiryDate: bid.recordAccessExpiryDate,
                                                    cancerType: bid.cancer_type,
                                                    recordId: record["_id"],
                                                    team: team,
                                                    patient: patient,
                                                };
                                                response.push(entry);
                                                cb();
                                            }
                                        }
                                    );
                                },
                                function (err) {
                                    callback();
                                }
                            );
                        } else {
                            var entry = {
                                bidName: bid.bidTitle,
                                bidId: bid._id,
                                numberOfRecordsAcquired: bid.recordsAcquired,
                                numberOfRecordsRequired: bid.recordsRequired,
                                amountPerRecord: bid.amountPerRecord,
                                recordAccessStartDate: bid.recordAccessStartDate,
                                recordAccessExpiryDate: bid.recordAccessExpiryDate,
                                cancerType: bid.cancer_type,
                            };
                            response.push(entry);
                            callback();
                        }
                    },
                    function (err) {
                        return res.status(200).json({
                            success: true,
                            message: "Successfully retrieved the bid",
                            data: response,
                        });
                    }
                );
            } else {
                return res.status(500).json({
                    success: true,
                    message: "No bids matured yet",
                });
            }
        })
        .catch((err) => {
            return res.status(500).json({
                success: true,
                message: "Error while retrieving the bid",
            });
        });
};

const stripeChargeCallback = res => (stripeErr, stripeRes) => {
    if (stripeErr) {
        res.status(500).send({
            error: stripeErr
        });
    } else {
        res.status(200).send({
            success: stripeRes
        });
    }
};

exports.buyerAcceptBids = function (req, res) {
    const userId = req.body.userId;
    const bidId = req.body.bidId;
    const recordIdList = req.body.recordIdList;
    const cardNumber = req.body.cardNumber;
    const cvc = req.body.cvc;
    var expiryDate = new Date(req.body.expiryDate)
    var expiry_month = expiryDate.getMonth() + 1; //Since month index are 0 based you have to increment it by 1.
    var expiry_year = expiryDate.getFullYear();

    if (!userId) {
        return res.status(500).json({
            success: true,
            message: "Invalid Access Token!",
        });
    }
    async.series(
        [
            function (cb) {
                User.findById(userId).then((result) => {
                    if (result) {
                        user = result;
                        cb();
                    } else {
                        cb("user not found");
                    }
                });
            },
            function (cb) {
                Bid.findById(bidId).then((result) => {
                    if (result) {
                        bid = result;
                        cb();
                    } else {
                        cb("Bid not found");
                    }
                });
            },
            function (cb) {
                stripe.tokens.create({
                        card: {
                            number: cardNumber,
                            exp_month: expiry_month,
                            exp_year: expiry_year,
                            cvc: cvc,
                            // number: '4242424242424242',
                            // exp_month: 5,
                            // exp_year: 2021,
                            // cvc: '314',
                        },
                    },
                    function (err, token) {
                        if (err) cb("Provide valid card details");
                        else {
                            stripeToken = token.id;
                            stripe.charges.create({
                                source: stripeToken,
                                amount: bid.amountPerRecord * bid.recordsRequired,
                                currency: 'usd',
                                description: 'Example charge',
                            }, function (err, payment) {
                                if (err) {
                                    cb("Payment Unsuccessfull");
                                } else {
                                    console.log('Success! payment with Stripe ID!');
                                    console.log(bid.amountPerRecord);
                                    cb();
                                }
                            });
                        }
                        // asynchronously called
                    }
                );
            },
            function (cb) {
                var recordsAccepted = bid.recordAccepted;
                const result = recordIdList.every((val) =>
                    recordsAccepted.includes(val)
                );
                if (result) {
                    async.each(
                        recordIdList,
                        function (recordId, callback) {
                            Record.findById(recordId)
                                .populate("team")
                                .exec(function (err, record) {
                                    if (err) callback("Couldnt find one of the records!");
                                    else {
                                        if (record) {
                                            payTheTeam(
                                                bid.amountPerRecord,
                                                bid.buyer,
                                                record.team,
                                                function (err, data) {
                                                    if (err) callback(err);
                                                    else callback();
                                                }
                                            );
                                        }
                                    }
                                });
                        },
                        function (err, data) {
                            if (err) cb(err);
                            else cb();
                        }
                    );
                } else {
                    cb("Some records werent accepted by the team");
                }
            },
            function (cb) {
                Bid.findByIdAndUpdate(bidId, {
                        recordBought: recordIdList,
                    })
                    .then((result) => {
                        cb();
                    })
                    .catch((err) => {
                        cb(err);
                    });
            },
        ],
        function (err, data) {
            if (err)
                return res.status(500).json({
                    success: false,
                    message: err,
                });
            else
                return res.status(200).json({
                    success: true,
                    message: "Successfully paid for the bid!",
                });
        }
    );
};

function payTheTeam(amount, buyer, team, callback) {
    async.series(
        [
            function (cb) {
                User.findById(buyer, function (err, result) {
                    if (err) cb("Error retrieving buyer!");
                    else {
                        if (result) {
                            var wallet = result.walletAmount - amount;
                            if (wallet >= 0)
                                User.findByIdAndUpdate(
                                    buyer, {
                                        walletAmount: wallet,
                                    },
                                    function (err, data) {
                                        if (err) cb("error while deducting money from the buyer");
                                        else {
                                            cb();
                                        }
                                    }
                                );
                            else cb("Buyer doesnt have enough money to buy the record!");
                        } else {
                            cb("Buyer doesnt exist!");
                        }
                    }
                });
            },
            function (cb) {
                var patient = team.admin;
                if (patient) {
                    User.findById(patient, function (err, result) {
                        if (err) cb("Error while finding the patient");
                        else {
                            if (result) {
                                var wallet =
                                    result.walletAmount + amount * (team.adminShare / 100);
                                User.findByIdAndUpdate(
                                    patient, {
                                        walletAmount: wallet,
                                    },
                                    function (err, result) {
                                        if (err) cb("error while adding amount to the patient");
                                        else cb();
                                    }
                                );
                            } else {
                                cb("No patient found");
                            }
                        }
                    });
                } else {
                    cb("patient not found");
                }
            },
            function (cb) {
                var members = team.members;
                if (members && members.length) {
                    async.each(
                        members,
                        function (member, calback) {
                            User.findById(member.user, function (err, result) {
                                if (err) calback("couldnt find one of the members!");
                                else {
                                    if (result) {
                                        var wallet =
                                            result.walletAmount + amount * (member.share / 100);
                                        User.findByIdAndUpdate(
                                            member.user, {
                                                walletAmount: wallet,
                                            },
                                            function (err, data) {
                                                if (err)
                                                    calback(
                                                        "Error hile updating the wallet amount for one of the members"
                                                    );
                                                else {
                                                    calback();
                                                }
                                            }
                                        );
                                    } else {
                                        calback();
                                    }
                                }
                            });
                        },
                        function (err, data) {
                            if (err) cb(err);
                            else cb();
                        }
                    );
                } else cb();
            },
        ],
        function (err, data) {
            if (err) callback(err);
            else callback();
        }
    );
}

exports.updateBidStatus = function (req, res) {
    const filter = {
        buyer: req.body.userId,
        _id: req.body.bidId,
        bidStartDate: {
            $lte: new Date()
        },
        bidExpiryDate: {
            $gte: new Date()
        },
    };
    const update = {
        bidStatus: constant.CONSTANTS.bidStatus.INACTIVE
    };

    Bid.findOneAndUpdate(filter, update)
        .then((result) => {
            return res.status(200).json({
                success: true,
                message: "Bid status updated successfully.",
            });
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                message: "Failed to update bid status.",
            });
        });
};

exports.editBid = function (req, res) {
    const userId = req.body.userId;
    const bidId = req.body.bidId;
    var update = {};
    if (req.body.bidStartDate) update.bidStartDate = req.body.bidStartDate;
    if (req.body.bidExpiryDate) update.bidExpiryDate = req.body.bidExpiryDate;
    if (req.body.recordAccessStartDate)
        update.recordAccessStartDate = req.body.recordAccessStartDate;
    if (req.body.recordAccessExpiryDate)
        update.recordAccessExpiryDate = req.body.recordAccessExpiryDate;
    if (req.body.amountPerRecord)
        update.amountPerRecord = req.body.amountPerRecord;
    if (req.body.recordsRequired)
        update.recordsRequired = req.body.recordsRequired;
    if (req.body.cancerType) update.cancerType = req.body.cancerType;
    if (!bidId) {
        return res.status(500).json({
            success: false,
            message: "Bid id hasnt been sent in request",
        });
    }

    Bid.find({
            _id: bidId,
            buyer: userId
        })
        .then((result) => {
            if (result && result.length) {
                if (result[0].bidStartDate <= new Date()) {
                    return res.status(500).json({
                        success: false,
                        message: "Bid has started already and cannot be edited!",
                    });
                } else {
                    Bid.findOneAndUpdate({
                            _id: bidId,
                            buyer: userId
                        }, update)
                        .then((result) => {
                            return res.status(200).json({
                                success: true,
                                message: "Bid has been successfully updated!",
                            });
                        })
                        .catch((err) => {
                            return res.status(500).json({
                                success: true,
                                message: err.message,
                            });
                        });
                }
            } else {
                return res.status(500).json({
                    success: false,
                    message: "Bid doesnt belong to the buyer!",
                });
            }
        })
        .catch((err) => {
            return res.status(500).json({
                success: false,
                message: err.message,
            });
        });
};

exports.createSingleBid = function (req, res) {
    const userId = req.body.userId;
    const recordId = req.body.recordId;

    if (!recordId) {
        return res.status(500).json({
            success: false,
            message: "Record id hasnt been sent in request",
        });
    }

    Record.findById({
        _id: recordId
    }).then((result) => {
        if (result) {
            const bid = new Bid({
                bidTitle: result.name + "_" + result.cancer_type + "_",
                cancerType: result.cancer_type,
                recordsRequired: 1,
                recordsAcquired: 0,
                bidStartDate: req.body.bidStartDate,
                bidExpiryDate: req.body.bidExpiryDate,
                amountPerRecord: req.body.amountPerRecord,
                bidStatus: req.body.bidStatus,
                recordAccessStartDate: req.body.recordAccessStartDate,
                recordAccessExpiryDate: req.body.recordAccessExpiryDate,
                buyer: userId,
                teamId: result.team,
            });
            bid
                .save()
                .then((result) => {
                    return res.status(200).json({
                        success: true,
                        message: "Bid successfully saved",
                        data: result,
                    });
                })
                .catch((err) => {
                    return res.status(500).json({
                        success: false,
                        message: err.message,
                    });
                });
        } else {
            return res.status(500).json({
                success: false,
                message: "No such record found",
            });
        }
    });
};