const User = require("../models/user.model");
const async = require("async");
const logger = require("../utils/logger");
var randomize = require("randomatic");
const WalletTransaction = require("../models/walletTransaction.model");
const nodemailer = require("nodemailer");
const config = require("config");

exports.sendCode = function (req, res) {
  var userId = req.body.userId;
  var accountNumber = req.body.accountNumber;
  var accountHolderName = req.body.accountHolderName;
  var amount = req.body.amount;
  var bsbNumber = req.body.bsbNumber;
  var verificationCode;
  var user = null;
  async.series(
    [
      function (cb) {
        if (userId) {
          User.findById(userId, function (err, result) {
            if (err) cb(err);
            else {
              if (
                result &&
                result.walletAmount &&
                result.walletAmount >= amount
              ) {
                user = result;
                cb();
              } else {
                cb("User doesn't enough necessary money in wallet to tranfer!");
              }
            }
          });
        } else {
          cb("User Id not given");
        }
      },
      function (cb) {
        if (user) {
          verificationCode = randomize("A0", 5);
          const walletTransaction = new WalletTransaction({
            userId: userId,
            accountHolderName: accountHolderName,
            accountNumber: accountNumber,
            amount: amount,
            bsbNumber: bsbNumber,
            confirmationCode: verificationCode,
          });
          walletTransaction.save({}, function (err, result) {
            if (err) cb(err);
            else {
              cb();
            }
          });
        } else {
          cb("User not found!");
        }
      },
      function (cb) {
        if (verificationCode && verificationCode.length === 5) {
          var email = config.get("email");
          var password = config.get("password");
          var transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
              user: email,
              pass: password,
            },
          });
          var content = `
            <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
            <html xmlns='http://www.w3.org/1999/xhtml'>
                <head> 
                    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /> 
                    <title>MyContext</title> 
                    <meta name='viewport' content='width=device-width, initial-scale=1.0' />
                </head>
                <body style='background: #ecf0f1'>
                    <p> Hello {{userName}},</p>
                    <p>Your verification code for money transfer from MyContext Wallet to account number ending with {{accNumLast}} is {{verificationCode}}</p>
                    <p>Please enter the code in the MyContext application to continue with the transaction</p>
                    <p>Thanks</p>
                    <p>My Context Team</p>
                </body>
            </html>
        `;
          content = content.replace("{{userName}}", user.name);
          content = content.replace(
            "{{accNumLast}}",
            accountNumber.substr(accountNumber.length - 4)
          );
          content = content.replace("{{verificationCode}}", verificationCode);
          var mailOptions = {
            from: email,
            to: user.email,
            subject:
              "MyContext - Verification code for the transferring money to your account",
            html: content,
          };
          transporter.sendMail(mailOptions, function (err, result) {
            if (err) cb(err);
            else cb();
          });
        } else {
          cb("Error occured while generating verification code!");
        }
      },
    ],
    function (err, result) {
      if (err) {
        logger.error(err);
        return res.status(500).json({
          success: false,
          message: err,
        });
      } else {
        return res.status(200).json({
          success: true,
          message: "Mail with verification code has been sent to ".concat(
            user.email
          ),
        });
      }
    }
  );
};

exports.transferMoney = function (req, res) {
  var userId = req.body.userId;
  var accountNumber = req.body.accountNumber;
  var accountHolderName = req.body.accountHolderName;
  var amount = req.body.amount;
  var bsbNumber = req.body.bsbNumber;
  var code = req.body.verificationCode;
  var transaction;
  async.series(
    [
      function (cb) {
        if (userId) {
          User.findById(userId, function (err, result) {
            if (err) cb(err);
            else {
              if (
                result &&
                result.walletAmount &&
                result.walletAmount >= amount
              ) {
                user = result;
                cb();
              } else {
                cb("User doesn't enough necessary money in wallet to tranfer!");
              }
            }
          });
        } else {
          cb("User Id not given");
        }
      },
      function (cb) {
        var query = {
          userId: userId,
          accountNumber: accountNumber,
          bsbNumber: bsbNumber,
          verified: false,
          transferred: false,
          confirmationCode: code,
        };
        WalletTransaction.findOne(query, function (err, result) {
          if (err) cb(err);
          else {
            if (result) {
              transaction = result;
              cb();
            } else {
              cb(
                "No transaction found with given verification code and accoutn details! You need to regenerate code if you have changed account details!"
              );
            }
          }
        });
      },
      function (cb) {
        // TODO: do payment on success below function happens
        cb();
      },
      function (cb) {
        var dataToUpdate = {
          verified: true,
          transferred: true,
          transferredDate: Date.now(),
        };
        WalletTransaction.findByIdAndUpdate(
          transaction._id,
          dataToUpdate,
          function (err, result) {
            if (err) cb(err);
            else {
              cb();
            }
          }
        );
      },
      function (cb) {
        var dataToUpdate = {
          walletAmount: user.walletAmount - amount,
        };
        User.findByIdAndUpdate(userId, dataToUpdate, function (err, result) {
          if (err) cb(err);
          else {
            cb();
          }
        });
      },
    ],
    function (err, result) {
      if (err) {
        logger.error(err);
        return res.status(500).json({
          success: false,
          message: err,
        });
      } else {
        return res.status(200).json({
          success: true,
          message: "The amount $"
            .concat(amount)
            .concat(" has been credited to the account ending with ")
            .concat(accountNumber.substr(accountNumber.length - 4)),
        });
      }
    }
  );
};
