const SubRecord = require("../models/subrecord.model");
const logger = require("../utils/logger");

exports.add = function (req, res) {
  const subRecord = new SubRecord({
    patient: req.body.patientId,
    team: req.body.teamId,
    doctor: req.body.userId,
    data: req.body.data,
  });

  subRecord
    .save()
    .then((result) => {
      return res.status(200).json({
        success: true,
        message: "Sub record added successfully",
        data: result,
      });
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};

exports.list = function (req, res) {
  const from = req.body.from || 0;
  const size = req.body.from || 10;
  const teamId = req.body.teamId;

  SubRecord.find({ team: teamId })
    .skip(from)
    .limit(size)
    .then((result) => {
      return res.status(200).json({
        success: true,
        data: result,
        message: "Sub medical records fetched successfully.",
      });
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};
