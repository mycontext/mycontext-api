const Team = require("../models/team.model");
const logger = require("../utils/logger");

exports.getDoctorTeams = function (req, res) {
  const userId = req.body.userId;
  const from = req.body.from || 0;
  const size = req.body.size || 10;

  Team.find({ "members.user": userId })
    .populate("admin")
    .skip(from)
    .limit(size)
    .then((result) => {
      return res.status(200).json({
        success: true,
        data: result,
        message: "Teams fetched successfully",
      });
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};

exports.updateMemberShare = function (req, res) {
  const userId = req.body.userId;
  const teamId = req.body.teamId;
  const memberId = req.body.memberId;
  const share = req.body.share || 0;

  Team.findOne({ _id: teamId, admin: userId })
    .then((team) => {
      if (team) {
        if (team.members) {
          const index = team.members.findIndex(
            (m) => m.user.toString() === memberId
          );

          if (index == -1) {
            return res.status(500).json({
              success: false,
              message: "No team member found with given id",
            });
          } else {
            const member = team.members[index];

            if (member.share > share) {
              team.adminShare += member.share - share;
            } else {
              team.adminShare -= share - member.share;

              if (team.adminShare < 0) {
                return res.status(500).json({
                  success: false,
                  message: "You doesn't have enough share to transfer",
                });
              }
            }

            team.members[index].share = share;

            team
              .save()
              .then((result) => {
                return res.status(200).json({
                  success: true,
                  message: "Share updated successfully",
                });
              })
              .catch((error) => {
                logger.error("error");

                return res.status(500).json({
                  success: false,
                  message: "No team member found with given id",
                });
              });
          }
        } else {
          return res.status(500).json({
            success: false,
            message: "No team member found with given id",
          });
        }
      } else {
        return res.status(500).json({
          success: false,
          message: "No team  found with the given id",
        });
      }
    })
    .catch((error) => {
      logger.error("error");

      return res.status(500).json({
        success: false,
        message: error.message,
      });
    });
};
