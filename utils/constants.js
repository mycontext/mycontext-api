/**
 * This fiel contains Constants defined as Javascript objects that are freezed to obtain enum kind of functionality
 */

var constants = {};
/**
 * Bid Status Enum to be used in the application
 */
constants.bidStatus = Object.freeze({ ACTIVE: "ACTIVE", INACTIVE: "INACTIVE" });

/**
 * Bid status constants to be used as enum in the database
 */
constants.DB_BIDSTATUS = ["ACTIVE", "INACTIVE"];

/**
 * User Type Enum to be used in the application
 */
constants.userTypes = Object.freeze({
  DOCTOR: "Doctor",
  PATIENT: "Patient",
  PHARMA: "Pharmaceutical Company",
  PATHOLOGIST: "Pathologist",
  PATHOLOGISTLAB: "Pathology Laboratory",
  INSURANCE: "Insurance Company",
  ORGANZATION: "Organization",
});

/**
 * Bid status constants to be used as enum in the database
 */
constants.DB_USERTYPES = [
  "Doctor",
  "Patient",
  "Organization",
  "Pharmaceutical Company",
  "Pathologist",
  "Pathology Laboratory",
  "Insurance Company",
];

module.exports = {
  CONSTANTS: constants,
};
